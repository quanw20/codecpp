#include <bits/stdc++.h>
using namespace std;
int main(int argc, char const* argv[]) {
	int a[] = { 0, 1, 2, 3, 3, 5, 6 };
	int l1 = lower_bound(a, a + 7, -1) - a;// >=val的第一个
	int l2 = lower_bound(a, a + 7, 7) - a;
	int u1 = upper_bound(a, a + 7, -1) - a;// >val的第一个
	int u2 = upper_bound(a, a + 7, 7) - a;
	int l3 = lower_bound(a, a + 7, 3) - a;
	int u3 = upper_bound(a, a + 7, 3) - a;
	cout << l1 << '\n';
	cout << u1 << '\n';
	cout << l2 << '\n';
	cout << u2 << '\n';
	cout << l3 << '\n';
	cout << u3 << '\n';
	return 0;
}