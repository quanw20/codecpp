/**
 * @file Main.cpp
 * @author 权威
 * @brief Flip Game
 * 翻转游戏是在一个 4x4 的长方形场地上进行的，在其 16 个方格中的每一个方格上都放置有两侧的棋子。
 * 每件的一面是白色的，另一面是黑色的，每件都是黑色或白色的一面朝上。
 * 每轮你翻转 3 到 5 块，从而将它们上侧的颜色从黑色变为白色，反之亦然。
 * 每轮都根据以下规则选择要翻转的棋子：
 * 		1.选择 16 件中的任何一件。
 * 		2.将选定的棋子以及所有相邻的棋子翻转到所选棋子的左侧、右侧、顶部和底部（如果有的话）。
 * @version 0.1
 * @date 2022-05-22
 *
 * @copyright Copyright (c) 2022
 *

 */
#include <iostream>
#include <algorithm>
using namespace std;
//每一行压缩为一个数字
int field[8];
// 1000, 0100, 0010, 0001,
// 1100, 1110, 0111, 0011
int pos[][4] = { { 8, 4, 2, 1 }, { 12, 14, 7, 3 } };
void flip(int i, int j) { //对(i, j)位置的棋子进行翻转
	--j;
	field[i - 1] ^= pos[0][j];
	field[i] ^= pos[1][j];
	field[i + 1] ^= pos[0][j];
}

bool check() { // 0或15: 全0或全1
	return (field[1] == 0 || field[4] == 15) &&
		   field[1] == field[2] &&
		   field[1] == field[3] &&
		   field[1] == field[4];
}

bool dfs(int n, int i, int j) { //翻 n 个棋子
	if (n == 0)
		return check(); //判断是否符合条件
	//从翻16个棋子开始逐一减少到一个棋子都不用翻
	++j; //下一列, 如果不加则会超时
	if (j > 4) {
		++i;
		j = 1;
	}
	if (i > 4) //如果超过棋盘大小则不可能
		return false;
	for (; i <= 4; ++i) {
		for (; j <= 4; ++j) {
			flip(i, j);			  //翻转
			if (dfs(n - 1, i, j)) //在当前基础上递归
				return true;
			flip(i, j); //回溯
		}
		j = 1; // 新一行
	}
	return false;
}
int main() {
	// read 下标从1开始
	for (int i = 1; i <= 4; i++) {
		for (int j = 1; j <= 4; j++) {
			field[i] <<= 1;
			if (getchar() == 'b') {
				field[i] |= 1; //与1相或,于是就只改变最后一位
			}
		}
		getchar(); //读取换行符号
	}
	// solve
	for (int i = 0; i <= 16; i++) {
		if (dfs(i, 1, 0)) {
			cout << i << '\n';
			exit(0); // 找到了直接退出
		}
	}
	cout << "Impossible" << '\n';
	return 0;
}