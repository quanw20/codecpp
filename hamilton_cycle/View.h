#include <iostream>
#include <conio.h>
using std::cout;
using std::endl;
class View {
public:
	// func
	View() = default;
	int showMainMenu() const;
	int showSubMenu() const;
	void exitSystem() const;
};
int View::showMainMenu() const {
	int cnt{ 0 }, c1, c2;
	while (1) {
		cout << R"( ________________________________________________________ )" << endl;
		cout << R"(|　By_Quanwei                               [－][口][×]| |)" << endl;
		cout << R"(|________________________________________________________|)" << endl;
		cout << R"(|               _欢迎使用旅游规划系统_                   |)" << endl;
		if (cnt == 0)
			cout << R"(|            ==> 0. 退出系统                             |)" << endl;
		else
			cout << R"(|                0. 退出系统                             |)" << endl;
		if (cnt == 1)
			cout << R"(|            ==> 1. 路径管理子系统                       |)" << endl;
		else
			cout << R"(|                1. 路径管理子系统                       |)" << endl;
		if (cnt == 2)
			cout << R"(|            ==> 2. 路径选择                             |)" << endl;
		else
			cout << R"(|                2. 路径选择                             |)" << endl;
		if (cnt == 3)
			cout << R"(|            ==> 3. 查找哈密顿回路                       |)" << endl;
		else
			cout << R"(|                3. 查找哈密顿回路                       |)" << endl;
		if (cnt == 4)
			cout << R"(|            ==> 4. 查找欧拉回路                         |)" << endl;
		else
			cout << R"(|                4. 查找欧拉回路                         |)" << endl;
		if (cnt == 5)
			cout << R"(|            ==> 5. 查找最短路径                         |)" << endl;
		else
			cout << R"(|                5. 查找最短路径                         |)" << endl;
		cout << R"(|                                                        |)" << endl;
		cout << R"(|                请输入选项:)" << cnt << R"(                            |)" << endl;
		cout << R"(|________________________________________________________|)" << endl;
		c1 = _getch();
		if (c1 == 13) // enter
			return cnt;
		if (c1 >= 49 && c1 < 56)
			return c1 - 48;
		c2 = _getch();
		switch (c2) {
		case 72: // up
			--cnt;
			break;
		case 80: // down
			++cnt;
			break;
		case 75: // left
		case 77: // right
			return cnt;
		}
		system("cls");
	}
}
int View::showSubMenu() const {
	int cnt{ 0 }, c1, c2;
	while (1) {
		cout << R"( ________________________________________________________ )" << endl;
		cout << R"(|　By_Quanwei                               [－][口][×]| |)" << endl;
		cout << R"(|________________________________________________________|)" << endl;
		cout << R"(|               _欢迎使用路径管理子系统_                 |)" << endl;
		if (cnt == 0)
			cout << R"(|            ==> 0. 退出系统                             |)" << endl;
		else
			cout << R"(|                0. 退出系统                             |)" << endl;
		if (cnt == 1)
			cout << R"(|            ==> 1. 回到上级菜单                         |)" << endl;
		else
			cout << R"(|                1. 回到上级菜单                         |)" << endl;
		if (cnt == 2)
			cout << R"(|            ==> 2. 显示路径信息                         |)" << endl;
		else
			cout << R"(|                2. 显示路径信息                         |)" << endl;
		if (cnt == 3)
			cout << R"(|            ==> 3. 添加路径信息                         |)" << endl;
		else
			cout << R"(|                3. 添加路径信息                         |)" << endl;
		if (cnt == 4)
			cout << R"(|            ==> 4. 修改路径信息                         |)" << endl;
		else
			cout << R"(|                4. 修改路径信息                         |)" << endl;
		if (cnt == 5)
			cout << R"(|            ==> 5. 查找路径信息                         |)" << endl;
		else
			cout << R"(|                5. 查找路径信息                         |)" << endl;
		if (cnt == 6)
			cout << R"(|            ==> 6. 删除路径信息	                         |)" << endl;
		else
			cout << R"(|                6. 删除路径信息	                         |)" << endl;
		cout << R"(|                                                        |)" << endl;
		cout << R"(|                请输入选项:)" << cnt << R"(                            |)" << endl;
		cout << R"(|________________________________________________________|)" << endl;
		c1 = _getch();
		if (c1 == 13) // enter
			return cnt;
		if (c1 >= 49 && c1 < 56)
			return c1 - 48;
		c2 = _getch();
		switch (c2) {
		case 72: // up
			--cnt;
			break;
		case 80: // down
			++cnt;
			break;
		case 75: // left
		case 77: // right
			return cnt;
		}
		system("cls");
	}
}
void View::exitSystem() const {
	system("cls");
	cout << R"( ________________________________________________________)" << endl
		 << R"(|　By_Quanwei                               [－][口][×]| |)" << endl
		 << R"(|________________________________________________________|)" << endl
		 << R"(|                   Are you sure quit?                   |)" << endl
		 << R"(|                                                        |)" << endl
		 << R"(|                                                        |)" << endl
		 << R"(|                        ----------                      |)" << endl
		 << R"(|                       |    YES   |                     |)" << endl
		 << R"(|                        ----------                      |)" << endl
		 << R"(|________________________________________________________|)" << endl;
	system("pause");
	cout << "欢迎下次使用 :)" << endl;
	exit(0);
}