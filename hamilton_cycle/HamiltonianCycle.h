#include <iostream> // cout
#include <vector>
#include <functional>
#include <memory> // unique_ptr
#include "AMGraph.h"
using VVB = std::vector<std::vector<bool>>;
/**
 * @brief哈密顿回路
 *
 */
class HamiltonianCycle {
private:
	VVB edge;
	std::vector<int> path;
	std::vector<bool> visited;
	std::vector<std::string> vertexs;

public:
	HamiltonianCycle(AMGraph& g) {
		int V = g.getVertexNum();
		visited.assign(V, false);
		edge = g.toMatrix();
		vertexs = g.getVertexs();
	}
	bool hasCycle(int vertex) {
		path.push_back(vertex);
		if (path.size() == edge.size() + 1 && vertex == path[0])
			return true;
		if (vertex == path[0] && path.size() != 1) {
			path.pop_back();
			return false;
		}
		for (size_t i = 0; i < edge.size(); ++i) {
			if (edge[vertex][i] && (!visited[i] || i == path[0])) {
				visited[vertex] = true;
				if (hasCycle(i))
					return true;
				visited[vertex] = false;
			}
		}
		path.pop_back();
		return false;
	}
	void print() {
		for (size_t i = 0; i < path.size(); ++i)
			std::cout << vertexs[path[i]]
					  << ((i != path.size() - 1) ? " -> " : " ");
	}
};