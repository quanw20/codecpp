#include "UUAMGraph.h"
#include "AMGraph.h"
#include "HamiltonianCycle.h"
#include "EularCycle.h"
#include "Dijkstra.h"
#include "View.h"
using namespace std;

class Controllor {
private:
	// 图的数量
	static const int N = 2;

	// fileName 数组
	string fn[N];

	// 图数组
	AMGraph* g[N];

	// 选择的图数组下标
	int gIndex;

	// 路径选择
	int pathSelect();

	// 查找哈密顿回路
	void showHamiltonCycle();

	// 查找欧拉回路
	void showEularCycle();

	// 查找最短路径
	void showDijkstraShortestPath();

public:

	// 初始化
	Controllor();

	//  执行功能
	int exec();
};
int Controllor::pathSelect() {
	for (int i = 0; i < N; ++i) {
		cout << *g[i] << "\n";
	}
	cout << "选择：  ";
	cin >> gIndex;
	return gIndex;
}
void Controllor::showHamiltonCycle() {
	HamiltonianCycle hc(*g[gIndex]);
	if (hc.hasCycle(0)) {
		cout << "图中存在的哈密顿回路为: \n";
		hc.print();
		cout << "\n";
	} else {
		cout << "图中不存在哈密顿回路\n";
	}
}
void Controllor::showEularCycle() {
	EularCycle ec(*g[gIndex]);
	if (ec.hasCycle()) {
		cout << "图中存在的欧拉回路为: \n";
		ec.print();
		cout << "\n";
	} else {
		cout << "图中不存在欧拉回路\n";
	}
}
void Controllor::showDijkstraShortestPath() {
	int src = 0;
	Dijkstra dj(*g[gIndex], src);
	auto path = dj.getPath();
	auto dist = dj.getDist();
	auto vertexs = (*g[gIndex]).getVertexs();
	cout << "到各点的距离: \n";
	for_each(dist.begin(), dist.end(), [](int x) { cout << x << ' '; });
	cout << "\n回溯的路径(下标): \n";
	for_each(path.begin(), path.end(), [](int x) { cout << x << ' '; });
	cout << "\n具体路径及长度(边权):\n";
	for (int i = 0; i < 5; ++i) {
		if (i == src)
			continue;
		cout << vertexs[src];
		dj.print(i);
		cout << " (" << dist[i] << ")\n";
	}
}
Controllor::Controllor() {
	fn[0] = "graph1.txt";
	fn[1] = "graph2.txt";
	for (int i = 0; i < N; ++i) {
		g[i] = new AMGraph(fn[i]);
	}
}
int Controllor::exec() {
	View view;
	try {
		while (true) {
		show_main_menu:
			int choice = view.showMainMenu();
			int ch2;
			if (choice == -5)
				throw "input error";
			switch (choice) {
			case 0: //退出系统
				view.exitSystem();
				goto exit_loop;
			case 1: { //路径管理子系统
				while (true) {
					ch2 = view.showSubMenu();
					switch (ch2) {
					case 0:
						view.exitSystem();
						goto exit_loop;
					case 1:
						goto show_main_menu;
					default:
						system("cls"); //刷新屏幕
						break;
					}
				}
				break;
			}
			case 2: //路径选择
				pathSelect();

				break;
			case 3: //查找哈密顿回路
				showHamiltonCycle();
				system("pause");
				break;
			case 4: //查找欧拉回路
				showEularCycle();
				system("pause");
				break;
			case 5: //查找最短路径
				showDijkstraShortestPath();
				system("pause");
				break;
			default:
				system("cls"); //刷新屏幕
				break;
			}
		}
	} catch (const char* error) {
		std::cout << error << endl;
	} catch (...) // catch all
	{
		std::cout << "error" << endl;
	}
exit_loop:
	return 0;
}
