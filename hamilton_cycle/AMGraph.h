#if !defined(__ADJACENCY_MATRIX_H__)
#define __ADJACENCY_MATRIX_H__
#include <iostream> // cout
#include <queue>	// queue
#include <fstream>	// ifstream
#include <cstring>	// menset
using VVI = std::vector<std::vector<int>>;
using VVB = std::vector<std::vector<bool>>;

/**
 * @brief 用邻接矩阵实现的带权值的有向图
 * @tparam std::string 顶点类型
 * @tparam _N 顶点个数
 */
class AMGraph {
private:
	int _N;								// 节点数
	int edgeNum;						// 边数
	std::vector<std::vector<int>> edge; // 邻接矩阵
	std::vector<std::string> vertexs;	// 存放节点值
	void dfs(char vis[], int u);		// dfs core

public:
	static const int INF;				// declare INF
	AMGraph(std::string fileName);
	void dfs();
	/**
	 * @brief 转换成用vector存储的二维bool数组 表示无向图
	 * @return std::vector<std::vector<bool>>
	 */
	VVB toMatrix();
	VVI getEdge() { return edge; }
	int getVertexNum() { return _N; }
	int getEdgeNum() { return edgeNum; }
	std::vector<std::string> getVertexs() { return vertexs; }
	friend std::ostream& operator<<(std::ostream& os, AMGraph& g); // 打印输出
	~AMGraph() {}
};
const int AMGraph::INF = 0xffff;		 // define INF
AMGraph::AMGraph(std::string fileName) { // 从模板参数获取节点数
	std::ifstream cin(fileName);
	cin >> _N; // 从文件中读取顶点
	edge.resize(_N, std::vector<int>(_N));
	vertexs.resize(_N);
	for (int i = 0; i < _N; ++i)
		cin >> vertexs[i];
	for (int i = 0; i < _N; ++i)
		for (int j = 0; j < _N; ++j)
			edge[i][j] = i == j ? 0 : INF;
	cin >> edgeNum; // 从文件中读取边
	int u, v, w;
	for (int i = 0; i < edgeNum; ++i) {
		cin >> u >> v >> w;
		edge[u][v] = w;
	}
}
void AMGraph::dfs(char vis[], int u) {
	vis[u] = 1;
	std::cout << vertexs[u] << ' ';
	for (int v = 0; v < _N; ++v)
		if (edge[u][v] != INF && edge[u][v] && !vis[v]) {
			std::cout << "-(" << edge[u][v] << ")-> ";
			dfs(vis, v);
		}
}

void AMGraph::dfs() {
	char vis[_N];					 // define
	std::memset(vis, 0, sizeof vis); // init
	for (int u = 0; u < _N; ++u)	 // do
		if (!vis[u])
			dfs(vis, u);
}
VVB AMGraph::toMatrix() {
	VVB ret(_N, std::vector<bool>(_N, false));
	for (int i = 0; i < _N; ++i)
		for (int j = 0; j < _N; ++j)
			if (edge[i][j] != INF && edge[i][j] != 0)
				ret[i][j] = true;
	return ret;
}
std::ostream& operator<<(std::ostream& os, AMGraph& g) {
	for (int i = 0; i < g._N; ++i)
		os << "\t" << g.vertexs[i];
	for (int i = 0; i < g._N; ++i) {
		os << '\n'
		   << g.vertexs[i];
		for (int j = 0; j < g._N; ++j) {
			if (g.edge[i][j] == g.INF)
				os << "\tINF";
			else
				os << '\t' << g.edge[i][j];
		}
	}
	return os;
}
#endif // __ADJACENCY_MATRIX_H__