#include <iostream> // cout
#include <vector>
#include <functional>
#include <memory> // unique_ptr
#include "AMGraph.h"
using VVB = std::vector<std::vector<bool>>;
/**
 * @brief 欧拉回路
 */
class EularCycle {
private:
	VVB edge;
	std::vector<int> path;
	std::vector<int> deg;
	std::vector<std::string> vertexs;
	int src;
	int edgeNum;
	bool hasEularCycle;

	void dfs(int src) {
		for (int i = 0; i < edgeNum; ++i) { //顺序寻找可访问的边，优先找编号小的节点
			if (edge[src][i]) {				//若这条边尚未访问过
				edge[src][i] = 0;			//已访问过的边要删去，防止重复访问
				edge[i][src] = 0;
				dfs(i);
			}
		}
		path.push_back(src); //将访问的节点储存进答案数组由于递归的特性，这里储存的是逆序过程
	}

public:
	EularCycle(AMGraph& g) {
		// init var
		vertexs = g.getVertexs();
		edgeNum = g.getEdgeNum();
		edge = g.toMatrix();
		int V = g.getVertexNum();
		deg.assign(V, 0);
		for (int i = 0; i < V; ++i) {
			for (int j = 0; j < V; ++j) {
				if (edge[i][j]) {
					++deg[i];
					++deg[j];
				}
			}
		}
		// 判断是否存在欧拉回路
		int cnt = 0;
		for (int i = 0; i < V; ++i) {
			if (deg[i] & 1) {
				++cnt;
			}
		}
		if (cnt == 0 || cnt == 2) { // init src
			hasEularCycle = true;
			for (int i = 0; i < V; ++i)
				if (deg[i]) { //寻找最小的有度的点即可
					src = i;
					break;
				}

			dfs(src);
		} else {
			hasEularCycle = false;
		}
	}

	bool hasCycle() {
		return hasEularCycle;
	}

	void print() {
		for (int i = path.size() - 1; i >= 0; --i)
			std::cout << vertexs[path[i]] << (i ? " -> " : " ");
	}
};