#include "UUAMGraph.h"
#include "View.h"
#include "AMGraph.h"
#include "HamiltonianCycle.h"
#include "EularCycle.h"
#include "Dijkstra.h"

using namespace std;

const std::string fileName = "graph1.txt";

string s[] = { "唐", "权", "威", "的", "图" };
Graph<string, 5> g(s);

void test_graph() {
	cout << "--用邻接矩阵实现的图: \n";
	cout << g << '\n';
	cout << "有环吗？\t"
		 << (g.isCyclic() ? "有 ~" : "没有 ~") << "\n";
	cout << "联通吗？\t"
		 << (g.isAllConnected() ? "联通 ~" : "不连通 ~") << "\n";
}

void test_hcycle() {
	cout << "\n--测试哈密顿回路: \n";
	unique_ptr<Graph<string, 5>::HamiltonianCycle>
		hc = g.getHamiltonianCycle();

	if (hc->hasCycle(0)) {
		cout << "图中存在的哈密顿回路为: \n";
		hc->print();
		cout << "\n";
	} else {
		cout << "图中不存在哈密顿回路\n";
	}
	hc.release();
}

void test_ecycle() {
	cout << "\n--测试欧拉回路: \n";
	unique_ptr<Graph<string, 5>::EularCycle>
		ec = g.getEularCycle();

	if (ec->hasCycle()) {
		cout << "图中存在的欧拉回路为: \n";
		ec->print();
		cout << "\n";
	} else {
		cout << "图中不存在欧拉回路\n";
	}
	ec.release();
}
void test_view() {
	View v;
	v.showMainMenu();
}
void test_amg() {
	AMGraph g(fileName);
	cout << g << "\n";
	g.dfs();
}
void HamiltonCycle() {
	AMGraph g(fileName);
	HamiltonianCycle hc(g);
	if (hc.hasCycle(0)) {
		cout << "图中存在的哈密顿回路为: \n";
		hc.print();
		cout << "\n";
	} else {
		cout << "图中不存在哈密顿回路\n";
	}
}
void test_ec2() {
	AMGraph g(fileName);
	EularCycle ec(g);
	if (ec.hasCycle()) {
		cout << "图中存在的欧拉回路为: \n";
		ec.print();
		cout << "\n";
	} else {
		cout << "图中不存在欧拉回路\n";
	}
}
void test_dj() {
	AMGraph g(fileName);
	int src = 0;
	Dijkstra dj(g, src);
	auto path = dj.getPath();
	auto dist = dj.getDist();
	auto vertexs = g.getVertexs();
	cout << "到各点的距离: \n";
	for_each(dist.begin(), dist.end(), [](int x) { cout << x << ' '; });
	cout << "\n回溯的路径(下标): \n";
	for_each(path.begin(), path.end(), [](int x) { cout << x << ' '; });
	cout << "\n具体路径及长度(边权):\n";
	for (int i = 0; i < 5; ++i) {
		if (i == src)
			continue;
		cout << vertexs[src];
		dj.print(i);
		cout << " (" << dist[i] << ")\n";
	}
}
int main() {

	// test_graph();

	// test_hcycle();

	// test_ecycle();
	// test_hc2();
	// test_ec2();
	test_dj();
	return 0;
}