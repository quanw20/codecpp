#ifndef __GRAPH_H__
#define __GRAPH_H__

#include <iostream> // cout
#include <fstream>	// ifstream
#include <cstring>	// menset
#include <vector>
#include <functional>
#include <memory> // unique_ptr

using std::cout;
using VVB = std::vector<std::vector<bool>>;

/**
 * @brief 用邻接矩阵实现的不带去权值无向图
 * (Unweighted Undirected  Adjacency Matrix Graph)
 * @tparam T 顶点类型
 * @tparam _N 顶点个数
 */
template <class T, int _N>
class Graph {
public:
	/**
	 * @brief 哈密顿回路
	 */
	class HamiltonianCycle {
	private:
		VVB edge;
		std::vector<int> path;
		std::vector<bool> visited;
		std::vector<T> vertexs;

	public:
		HamiltonianCycle(Graph* g) {
			visited.assign(_N, false);
			edge = g->toMatrix();
			vertexs = g->getVertexs();
		}
		bool hasCycle(int vertex) {
			path.push_back(vertex);
			if (path.size() == _N + 1 && vertex == path[0])
				return true;
			if (vertex == path[0] && path.size() != 1) {
				path.pop_back();
				return false;
			}
			for (size_t i = 0; i < _N; ++i) {
				if (edge[vertex][i] && (!visited[i] || i == path[0])) {
					visited[vertex] = true;
					if (hasCycle(i))
						return true;
					visited[vertex] = false;
				}
			}
			path.pop_back();
			return false;
		}
		void print() {
			for (size_t i = 0; i < path.size(); ++i)
				cout << vertexs[path[i]]
					 << ((i != path.size() - 1) ? " -> " : " ");
		}
	};

	/**
	 * @brief 欧拉回路
	 */
	class EularCycle {
	private:
		VVB edge;
		std::vector<int> path;
		std::vector<int> deg;
		std::vector<T> vertexs;
		int src;
		int edgeNum;
		bool hasEularCycle;

		void dfs(int src) {
			for (int i = 0; i < edgeNum; ++i) { //顺序寻找可访问的边，优先找编号小的节点
				if (edge[src][i]) {				//若这条边尚未访问过
					edge[src][i] = 0;			//已访问过的边要删去，防止重复访问
					edge[i][src] = 0;
					dfs(i);
				}
			}
			path.push_back(src); //将访问的节点储存进答案数组由于递归的特性，这里储存的是逆序过程
		}

	public:
		EularCycle(Graph* g) {
			// init var
			vertexs = g->getVertexs();
			edgeNum = g->getEdgeNum();
			edge = g->toMatrix();
			deg.assign(_N, 0);
			for (int i = 0; i < _N; ++i) {
				for (int j = 0; j < _N; ++j) {
					if (edge[i][j]) {
						++deg[i];
						++deg[j];
					}
				}
			}
			// 判断是否存在欧拉回路
			int cnt = 0;
			for (int i = 0; i < _N; ++i) {
				if (deg[i] & 1) {
					++cnt;
				}
			}
			if (cnt == 0 || cnt == 2) { // init src
				hasEularCycle = true;
				for (int i = 0; i < _N; ++i)
					if (deg[i]) { //寻找最小的有度的点即可
						src = i;
						break;
					}

				dfs(src);
			} else {
				hasEularCycle = false;
			}
		}

		bool hasCycle() {
			return hasEularCycle;
		}

		void print() {
			for (int i = path.size() - 1; i >= 0; --i)
				cout << vertexs[path[i]] << (i ? " -> " : " ");
		}
	};

private:
	// 栈上分配更快
	bool edge[_N][_N];

	// 边数
	int edgeNum;

	// 存放节点值
	T vertexs[_N];

	// dfs core
	void dfs(char vis[], int u, std::function<void(T)> cb);

	//判断是否有环
	bool isCyclicDfs(bool* vis, bool* path, int u);

public:
	/**
	 * @brief 传入元素个数等于顶点数的数组构造
	 * @param a 顶点数组
	 */
	Graph(T a[]);
	//todo
	Graph(std::vector<T> vertexs, VVB edge);

	/**
	 * @brief 转换成用vector存储的二维数组
	 * @return std::vector<std::vector<bool>>
	 */
	VVB toMatrix();

	/**
	 * @brief 获取边数
	 * @return int
	 */
	int getEdgeNum() { return edgeNum; }

	/**
	 * @brief 获取顶点数组
	 * @return std::vector<T>
	 */
	std::vector<T> getVertexs();

	/**
	 * @brief 判断是否有环
	 * @return true
	 * @return false
	 */
	bool isCyclic();

	/**
	 * @brief 判断是否是连通图
	 * @return true
	 * @return false
	 */
	bool isAllConnected();

	/**
	 * @brief 获取哈密顿回路对象
	 * 使用unique_ptr防内存泄漏
	 * 直接返回HamiltonianCycle对象开销大，使用move
	 * @return std::unique_ptr<HamiltonianCycle>
	 */
	std::unique_ptr<HamiltonianCycle> getHamiltonianCycle();

	/**
	 * @brief 获取欧拉回路对象
	 * @return std::unique_ptr<EularCycle>
	 */
	std::unique_ptr<EularCycle> getEularCycle();

	template <class Q, int W> // 打印输出
	friend std::ostream& operator<<(std::ostream& os, Graph<Q, W>& g);
};

template <class T, int _N>
Graph<T, _N>::Graph(T a[]) { // 从模板参数获取节点数
	std::ifstream cin("./hc.txt");
	for (int i = 0; i < _N; ++i)
		vertexs[i] = a[i];
	for (int i = 0; i < _N; ++i)
		for (int j = 0; j < _N; ++j)
			edge[i][j] = 0;
	cin >> edgeNum; // 从文件中读取边
	int u, v;
	for (int i = 0; i < edgeNum; ++i) {
		cin >> u >> v;
		edge[u][v] = true;
		edge[v][u] = true;
	}
}

template <class T, int _N>
Graph<T, _N>::Graph(std::vector<T> vertexs, VVB edge) {
	// int V = vertexs.size();
	// for (int i = 0; i < V; i++) {
	// 	for (int j = 0; j < V; j++) {
	// 		this->edge
	// 	}
	// }
}
template <class T, int _N>
std::vector<T> Graph<T, _N>::getVertexs() {
	std::vector<T> ret;
	for (int i = 0; i < _N; ++i)
		ret.push_back(vertexs[i]);
	return ret;
}

template <typename T, int _N>
void Graph<T, _N>::dfs(char vis[], int u, std::function<void(T)> cb) {
	cb(vertexs[u]);
	vis[u] = 1;
	for (int v = 0; v < _N; ++v) {
		if (edge[u][v] && !vis[v])
			dfs(vis, v, cb);
	}
}

template <class T, int _N>
bool Graph<T, _N>::isAllConnected() {
	int cnt = 0;
	char vis[_N];
	memset(vis, 0, _N);
	for (int i = 0; i < _N; ++i)
		if (!vis[i]) {
			++cnt;
			dfs(vis, i, [](T t) {});
		}
	return cnt == 1;
}

template <typename T, int _N>
bool Graph<T, _N>::isCyclicDfs(bool* vis, bool* path, int u) {
	if (!vis[u]) { // 没访问->访问&加入路径
		vis[u] = path[u] = true;
		for (int v = 0; v < _N; ++v)
			if (edge[u][v] && !vis[v] &&
					isCyclicDfs(vis, path, v) ||
				path[v])
				return true; // 没访问->访问||访问过&在栈中->true
	}
	return (path[u] = false); // 访问过&弹出栈
}

template <class T, int _N>
bool Graph<T, _N>::isCyclic() {
	bool vis[_N], path[_N]; // 环:遇到已经访问过&在栈中的顶点
	memset(vis, 0, sizeof vis);
	memset(path, 0, sizeof path);
	for (int i = 0; i < _N; ++i) // n
		if (!vis[i] && isCyclicDfs(vis, path, i))
			return true;
	return false;
}

template <class T, int _N>
VVB Graph<T, _N>::toMatrix() {
	VVB ret(_N, std::vector<bool>(_N, false));
	for (int i = 0; i < _N; ++i)
		for (int j = 0; j < _N; ++j)
			ret[i][j] = edge[i][j];
	return ret;
}

template <class T, int _N>
std::unique_ptr<typename Graph<T, _N>::HamiltonianCycle>
Graph<T, _N>::getHamiltonianCycle() {
	// 直接返回HamiltonianCycle对象开销大，使用move
	return std::make_unique<HamiltonianCycle>(this);
}

template <class T, int _N>
std::unique_ptr<typename Graph<T, _N>::EularCycle>
Graph<T, _N>::getEularCycle() {
	return std::make_unique<EularCycle>(this);
}

template <class Q, int W>
std::ostream& operator<<(std::ostream& os, Graph<Q, W>& g) {
	for (int i = 0; i < W; ++i)
		os << "\t" << g.vertexs[i];
	for (int i = 0; i < W; ++i) {
		os << "\n"
		   << g.vertexs[i];
		for (int j = 0; j < W; ++j)
			os << "\t" << g.edge[i][j];
	}
	return os;
}
#endif