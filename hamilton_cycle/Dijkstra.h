#include <bits/stdc++.h>
#include "AMGraph.h"
using std::vector;
using std::stack;
using std::cout;
using std::string;
using std::ifstream;
using std::function;
using VVI = vector<vector<int>>; // 重命名
using PII = std::pair<int, int>;
class Dijkstra {
private:
	int src;		  // 源点
	vector<int> path; // 到各点的路径
	vector<int> dist; // 到各点的距离
	vector<std::string> vertexs;

public:
	Dijkstra(AMGraph& g, int src) {
		this->src = src;
		VVI edge = g.getEdge();
		int V = g.getVertexNum();
		vertexs = g.getVertexs();
		path.resize(V);
		for (int i = 0; i < V; ++i) // O(n)
			dist.push_back(AMGraph::INF);
		dist[src] = 0;
		bool vis[V];
		memset(vis, 0, sizeof vis);
		std::priority_queue<PII> pq;
		pq.push({ 0, src });
		while (!pq.empty()) { // n
			PII p = pq.top(); // logn
			pq.pop();
			int u = p.second;
			if (vis[u])
				continue;
			vis[u] = true;
			for (int v = 0; v < V; ++v) { // e
				int d = dist[u] + edge[u][v];
				if (edge[u][v] != AMGraph::INF && edge[u][v] &&
					dist[v] > d) {
					dist[v] = d;
					path[v] = u;
					pq.push({ d, v });
				}
			}
		}
	}
	vector<int> getDist() { return dist; }
	vector<int> getPath() { return path; }
	void print(int e) { // 用来打印路径
		if (src == e)
			return;
		if (dist[e] == AMGraph::INF) {
			std::cout << "->" << vertexs[e];
			return;
		}
		print(this->path[e]);
		std::cout << "->" << vertexs[e];
	}
};
