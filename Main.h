#include <iostream>
#include <cstring>
#include <queue>				//priority_queue
#define PII std::pair<int, int> // 重命名

using namespace std;
const int MaxSize = 10;
int visited[MaxSize] = { 0 };
template <class T>
class MGraph {
public:
	MGraph(T a[], int n, int e); //构造函数
	~MGraph();					 //析构函数
	void DFTraverse(int v);		 //深度优先遍历
	void BFTraverse(int v);		 //广度优先遍历
	void Prim(int v);			 //求最小生成树
	int Fenliang();				 //判断连通性求连通分量
	void dfs(bool* vis, int i);

private:
	T vertex[MaxSize];			//存放图中顶点的数组
	int edge[MaxSize][MaxSize]; //存放图中边的数组
	int vertexNum, edgeNum;		//图的顶点数和边数
};
template <class T>
MGraph<T>::MGraph(T a[], int n, int e) {
	int i, j, k;
	vertexNum = n;
	edgeNum = e;
	for (i = 0; i < vertexNum; i++)
		vertex[i] = a[i];
	for (i = 0; i < vertexNum; i++)
		for (j = 0; j < vertexNum; j++)
			edge[i][j] = 0;
	for (k = 0; k < edgeNum; k++) {
		cin >> i >> j;
		edge[i][j] = 1;
		edge[j][i] = 1;
	}
}
template <class T>
MGraph<T>::~MGraph() {}
template <class T>
void MGraph<T>::DFTraverse(int v) {
	cout << vertex[v];
	visited[v] = 1;
	for (int j = 0; j < vertexNum; j++)
		if (edge[v][j] == 1 && visited[j] == 0)
			DFTraverse(j);
}
int count = 0;
template <class T>
int MGraph<T>::Fenliang() {
	int cnt = 0;
	bool vis[vertexNum];
	memset(vis, 0, vertexNum);
	for (int i = 0; i < vertexNum; ++i) {
		if (!vis[i]) {
			++cnt;
			dfs(vis, i);
		}
	}
	return cnt;
}
template <class T>
void MGraph<T>::dfs(bool* vis, int i) {
	vis[i] = true;
	// std::cout << vertex[i] << ' ';// just for debug
	for (int j = 0; j < vertexNum; ++j)
		if (!vis[j] && edge[i][j])
			dfs(vis, j);
}
template <class T>
void MGraph<T>::BFTraverse(int v) {
	int w, j, Q[MaxSize];
	int front = -1, rear = -1;
	cout << vertex[v];
	visited[v] = 1;
	Q[++rear] = v;
	while (front != rear) {
		w = Q[++front];
		for (j = 0; j < vertexNum; j++) {
			if (edge[w][j] == 1 && visited[j] == 0) {
				cout << vertex[j];
				visited[j] = 1;
				Q[++rear];
			}
		}
	}
}
int MinEdge(int* lowcost, int num);
template <class T>
void MGraph<T>::Prim(int v) {
	int i, j, k;
	int adjvex[vertexNum], lowcost[vertexNum]; //邻接点和权值
	for (i = 0; i < vertexNum; i++) {
		lowcost[i] = edge[v][i];
		adjvex[i] = v;
	}
	lowcost[v] = 0; // 不需要
	for (k = 1; k < vertexNum; k++) {
		j = MinEdge(lowcost, vertexNum);
		cout << j << " " << adjvex[j] << " " << lowcost[j] << endl;
		lowcost[j] = 0;
		for (i = 0; i < vertexNum; i++) {
			if (edge[j][i] < lowcost[i]) {
				lowcost[i] = edge[j][i];
				adjvex[i] = j;
			}
		}
	}
}
int MinEdge(int* lowcost, int num) {
	int min = 100000;
	int index = -1;
	for (int i = 0; i < num; i++) {
		if (lowcost[i] < min && lowcost[i] > 0) {
			min = lowcost[i];
			index = i;
		}
	}
	return index;
}
