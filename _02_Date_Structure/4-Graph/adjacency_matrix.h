#if !defined(__ADJACENCY_MATRIX_H__)
#define __ADJACENCY_MATRIX_H__
#include <iostream>			// cout
#include <queue>			// queue
#include <fstream>			// ifstream
#include <cstring>			// menset
#include "adjacency_list.h" // totoAdjacencyList

/**
 * @brief 用邻接矩阵实现的带权值的有向图
 * @tparam T 顶点类型
 * @tparam _N 顶点个数
 */
template <class T, int _N>
class AMGraph {
private:
	static const int INF;		 // declare INF
	int edge[_N][_N], edgeNum;	 // 栈上分配更快
	T vertexs[_N];				 // 存放节点值
	void dfs(char vis[], int u); // dfs core
	AMGraph();					 // 不用这个

public:
	AMGraph(T a[]);				  // 传入元素个数等于顶点数的数组构造
	void dfs();					  // 深度优先遍历
	void bfs();					  // 广度优先遍历
	ALGraph<T> toAdjacencyList(); // 转换为邻接表
	template <class Q, int W>	  // 打印输出
	friend std::ostream& operator<<(std::ostream& os, AMGraph<Q, W>& g);
	~AMGraph() {}
};
template <class T, int _N>
const int AMGraph<T, _N>::INF = 0xffff; // define INF
template <class T, int _N>
AMGraph<T, _N>::AMGraph(T a[]) { // 从模板参数获取节点数
	for (int i = 0; i < _N; ++i)
		vertexs[i] = a[i];
	for (int i = 0; i < _N; ++i)
		for (int j = 0; j < _N; ++j)
			edge[i][j] = i == j ? 0 : INF;
	std::ifstream cin("./adjacency_matrix.txt");
	cin >> edgeNum; // 从文件中读取边
	int u, v, w;
	for (int i = 0; i < edgeNum; ++i) {
		cin >> u >> v >> w;
		edge[u][v] = w;
	}
}
template <class T, int _N>
void AMGraph<T, _N>::dfs(char vis[], int u) {
	vis[u] = 1;
	std::cout << vertexs[u] << ' ';
	for (int v = 0; v < _N; ++v)
		if (edge[u][v] != INF && edge[u][v] && !vis[v]) {
			std::cout << "-(" << edge[u][v] << ")-> ";
			dfs(vis, v);
		}
}

template <class T, int _N>
void AMGraph<T, _N>::dfs() {
	char vis[_N];					 // define
	std::memset(vis, 0, sizeof vis); // init
	for (int u = 0; u < _N; ++u)	 // do
		if (!vis[u])
			dfs(vis, u);
}
template <class T, int _N>
void AMGraph<T, _N>::bfs() {
	std::queue<int> q;				 // define
	char vis[_N];					 // define
	std::memset(vis, 0, sizeof vis); // init
	for (int u = 0; u < _N; ++u) {	 // 避免非联通
		if (!vis[u]) {
			q.push(u);
			while (!q.empty()) {
				int v = q.front();
				q.pop();
				if (!vis[v]) { //没访问就访问
					std::cout << vertexs[v] << ' ';
					vis[v] = 1;
				}
				for (int w = 0; w < _N; ++w) // 添加可访问的
					if (edge[v][w] && edge[v][w] != INF && !vis[w])
						q.push(w);
			}
		}
	}
}
template <class T, int _N>
ALGraph<T> AMGraph<T, _N>::toAdjacencyList() { // 转换为邻接表
	ALGraph<T> ret(this->vertexs, _N);		   // 定义
	for (int i = 0; i < _N; ++i)			   // 添加边
		for (int j = 0; j < _N; ++j) {
			int w = this->edge[i][j];
			if (w && w != this->INF)
				ret.addEdge(vertexs[i], vertexs[j], w);
		}
	return ret;
}
template <class Q, int W>
std::ostream& operator<<(std::ostream& os, AMGraph<Q, W>& g) {
	for (int i = 0; i < W; ++i)
		os << "\t" << g.vertexs[i];
	for (int i = 0; i < W; ++i) {
		os << '\n'
		   << g.vertexs[i];
		for (int j = 0; j < W; ++j) {
			if (g.edge[i][j] == g.INF)
				os << "\tINF";
			else
				os << '\t' << g.edge[i][j];
		}
	}
	return os;
}
#endif // __ADJACENCY_MATRIX_H__