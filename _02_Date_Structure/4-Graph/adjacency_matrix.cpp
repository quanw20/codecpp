#include "adjacency_matrix.h"
using namespace std;
string s[] = { "tang", "quan", "wei", "唐", "权", "威", "T", "Q", "W" };
AMGraph<string, 9> g(s);

int main(int argc, char const* argv[]) {
	cout << "--用邻接矩阵实现的图: \n";
	cout << g << '\n';
	cout << "\n--图的深度优先遍历: \n";
	g.dfs();
	cout << "\n\n--图的广度优先遍历: \n";
	g.bfs();
	cout << "\n\n--将邻接矩阵转换为邻接表(一行是一个顶点的所有边和权): \n";
	ALGraph<string> alg = g.toAdjacencyList();
	cout << alg;
	return 0;
}