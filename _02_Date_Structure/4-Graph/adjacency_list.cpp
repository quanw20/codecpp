#include "adjacency_list.h"
using std::for_each;
char a[] = { 'T', 'A', 'N', 'G', 'Q', 'U', 'A', 'N', 'W' };
ifstream cin("adjacency_matrix.txt");
ALGraph<char> g(a, sizeof(a) / sizeof(a[0]), cin);

void test_triversal() {
	cout << "\n- 测试遍历 -\n";
	cout << "图g:" << '\n';
	cout << g;
	cout << "\n深度优先遍历\n";
	g.dfs([](ALGraph<char>::Vertex& x) { cout << x.val << ' '; });
	cout << "\n广度优先遍历\n";
	g.bfs([](ALGraph<char>::Vertex& x) { cout << x.val << ' '; });
}

void test_add_remove() {
	cout << "\n\n- 测试顶点和边的添加删除 -\n";
	cout << "添加顶点 x: " << (g.addVertex('x') ? "True" : "False") << '\n';
	cout << g;
	cout << "添加边 'x'->'T' w=100:" << (g.addEdge('x', 'T', 100) ? "True" : "False") << '\n';
	cout << g;
	cout << "移除存在边的节点 x: " << (g.removeVertex('x') ? "True" : "False") << '\n';
	cout << g;
	cout << "移除边 'x'->'T': " << (g.removeEdge('x', 'T') ? "True" : "False") << '\n';
	cout << g;
	cout << "移除不存在边的节点: " << (g.removeVertex('x') ? "True" : "False") << '\n';
	cout << g;
}

void test_mst() {
	cout << "\n\n- 测试最小生成树 -\n";
	cout << "-测试Kruskal算法:\n";
	ALGraph<char>::MST m1 = g.mstKruskal();
	cout << "权值: " << m1.weight << '\n';
	cout << "得到的最小生成树:\n";
	for_each(m1.edges.begin(), m1.edges.end(), [](ALGraph<char>::Triple x) { cout << '(' << x.from << ", " << x.to << ", " << x.weight << ") "; });
	cout << "\n-测试Prim算法:\n";
	ALGraph<char>::MST m2 = g.mstPrim();
	cout << "权值: " << m2.weight << '\n';
	cout << "得到的最小生成树:\n";
	for_each(m2.edges.begin(), m2.edges.end(), [](ALGraph<char>::Triple x) { cout << '(' << x.from << ", " << x.to << ", " << x.weight << ") "; });
}

void test_CC() {
	cout << "\n\n- 测试联通分支 -\n";
	auto cc = g.connectedComponet();
	cout << "联通分支数: " << cc.count << '\n';
	cout << "分支头顶点:\n";
	for_each(cc.vertexs.begin(), cc.vertexs.end(), [](ALGraph<char>::Vertex x) { cout << x.val << '\n'; });
}

void test_toMatrix() {
	cout << "\n\n- 测试转换为邻接矩阵 -\n";
	auto m = g.toAdjcencyMatrix();
	for_each(m.begin(), m.end(), [](vector<int>& x) {
		for_each(x.begin(), x.end(), [](int i) {
			if (i == ALGraph<char>::INF)
				cout << "INF\t";
			else
				cout << i << '\t';
		});
		cout << '\n';
	});
}

void test_SSP() {
	cout << "\n\n- 测试最短路 -\n";
	cout << "-使用Dijkstra算法:\n";
	int src = 0;
	cout << "起始顶点: " << a[src] << '\n';
	auto sp = g.spDijkstra(a[src]);
	cout << "到各点的距离: \n";
	for_each(sp.dist.begin(), sp.dist.end(), [](int x) { cout << x << ' '; });
	cout << "\n回溯的路径(下标): \n";
	for_each(sp.path.begin(), sp.path.end(), [](int x) { cout << x << ' '; });
	cout << "\n具体路径及长度(边权):\n";
	for (int i = 0; i < sizeof(a) / sizeof(a[0]); ++i) {
		if (i == src)
			continue;
		cout << a[src];
		sp.print(i, a);
		cout << " (" << sp.dist[i] << ")\n";
	}
}

void test_MSP() {
	cout << "-使用Floyd算法:\n";
	auto msp = g.spFloyd();
	cout << "多元最短路(长度):\n\t";
	int cnt = 0;
	for (int i = 0; i < msp.path.size(); ++i)
		cout << a[i] << "\t\t";
	for_each(msp.dist.begin(), msp.dist.end(), [&](vector<int>& x) {
		cout << "\n"
			 << a[cnt++] << ":";
		for_each(x.begin(), x.end(), [](int i) {
			if (i == ALGraph<char>::INF)
				cout << '\t' << "INF\t";
			else
				cout << '\t' << i << '\t';
		});
	});
	cout << "\n多元最短路(具体路径):\n\t";
	for (int i = 0; i < msp.path.size(); ++i)
		cout << a[i] << "\t\t";
	cnt = 0;
	for_each(msp.path.begin(), msp.path.end(), [&](vector<string>& x) {
		cout << "\n"
			 << a[cnt++] << ":";
		for_each(x.begin(), x.end(), [](string& i) {
			cout << "\t" << i << "\t";
		});
	});
}
void test_isCyclic() {
	cout << "\n\n- 测试是否有环 -\n";
	cout << "是否有环: " << (g.isCyclic() ? "True" : "False") << '\n';
}
void test_topoSort() {
	cout << "\n\n- 测试拓扑排序 -\n";
	cout << "-非递归拓扑排序:\n";
	auto tp = g.topologicalSort();
	cout << "-拓扑序列:\n";
	for_each(tp.begin(), tp.end(), [](int x) { cout << x << ' '; });
	cout << "\n-递归拓扑排序:\n";
	auto to = g.topologicalSortRecursive();
	cout << "-拓扑序列:\n";
	for_each(to.begin(), to.end(), [](int x) { cout << x << ' '; });
}
void test_LP() {
	cout << "\n\n- 测试最长路径 -\n";
	int src = 0;
	cout << "起始顶点: " << a[src] << '\n';
	auto slp = g.longsetPath(a[src]);
	cout << "到各点的最长距离: \n";
	for_each(slp.dist.begin(), slp.dist.end(), [](int x) { cout << x << ' '; });
	cout << "\n回溯的路径(下标): \n";
	for_each(slp.path.begin(), slp.path.end(), [](int x) { cout << x << ' '; });
	cout << "\n具体路径及长度(边权):\n";
	for (int i = 0; i < sizeof(a) / sizeof(a[0]); ++i) {
		if (i == src)
			continue;
		cout << a[src];
		slp.print(i, a);
		cout << " (" << slp.dist[i] << ")\n";
	}
}
void test_CP() {
	cout << "\n\n- 测试关键路径 -\n";
	ALGraph<char>::CP cp = g.criticalPath();
	cout << "关键活动: \n";
	for_each(cp.edges.begin(), cp.edges.end(), [](ALGraph<char>::Triple tp) { cout << "(" << tp.from << ", " << tp.to << ") "; });
	cout << "\n关键路径: \n";
	cp.print(a);
	cout << "\n长度: " << cp.weight << '\n';
}


int main() {
	// test_triversal();
	// test_add_remove();
	// test_mst();
	// test_CC();
	// test_toMatrix();
	// test_SSP();
	// test_MSP();
	// test_isCyclic();
	// test_topoSort();
	// test_LP();
	test_CP();
	return 0;
}