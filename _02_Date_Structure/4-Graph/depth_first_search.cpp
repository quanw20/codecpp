#include <bits/stdc++.h>
using namespace std;
void swap(int* a, int i, int j) {
	int temp = a[i];
	a[i] = a[j];
	a[j] = temp;
}
void dfs(int* a, int start, int end) {
	if (start >= end) {
		for (int i = 0; i < end; ++i)
			cout << a[i] << ' ';
		cout << '\n';
	}
	for (int i = start; i < end; ++i) {
		swap(a, i, start);
		dfs(a, start + 1, end);
		swap(a, i, start);
	}
}
void perm1(int n) {
	int a[n];
	for (int i = 0; i < n; ++i)
		a[i] = i + 1;
	dfs(a, 0, n);
}
int n; //
int path[10001];
int used[10001];
void perm2(int u) {
	if (u == n) {
		for (int i = 0; i < n; ++i)
			printf("%d ", path[i]);
		puts("");
		return;
	}
	for (int i = 1; i <= n; ++i) {
		if (!used[i]) {
			path[u] = i;
			used[i] = 1;
			perm2(u + 1);
			used[i] = 0;
		}
	}
}

int main(int argc, char const* argv[]) {
	n = 4;	  // 可改动
	perm2(0); // 固定
	return 0;
}