#include <bits/stdc++.h>
using namespace std;
struct Node {
	int val;
	Node* next;
	Node(int v, Node* n) : val(v), next(n) {}
};
const int MaxSize = 13;
Node* ht[MaxSize];
int hash(int k) { return k % MaxSize; }
void insert(int k) {
	int h = ::hash(k); 
	for (Node* p = ht[h]; p; p = p->next)
		if (p->val == k)
			return;
	ht[h] = new Node(k, ht[h]);
}
Node* sarch(int k) {
	int h = ::hash(k);
	Node* p = ht[h];
	for (Node* p = ht[h]; p; p = p->next)
		if (p->val == k)
			return p;
	return nullptr;
}
int main(int argc, char const* argv[]) {

	return 0;
}