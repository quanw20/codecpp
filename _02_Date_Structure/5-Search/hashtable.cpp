#include <bits/stdc++.h>
using namespace std;
const int MaxSize = 13;	 // 一个素数
int ht[MaxSize] = { 0 }; // 哈希表
/**
 * @brief 简单的哈希函数（使用除留余数法）
 *
 * @param k 键值
 * @return int hash code
 */
int hash(int k) {
	return k % MaxSize;
}
/**
 * @brief 插入
 *
 * @param k 键
 * @return true 插入成功
 * @return false 插入失败：表满了或者键重复了
 */
bool insert(int k) {
	int h = ::hash(k), i = h; // 这里可能跟标准库的hash函数重名了
	while (ht[i]) {
		if (ht[i] == k)
			return true;
		i = (i + 1) % MaxSize;
		if (i == h)
			return false;
	}
	ht[i] = k;
	return true;
}
/**
 * @brief 搜索
 *
 * @param k 键
 * @return int 在哈希表中的下标 （-1表示不存在）
 */
int search(int k) {
	int h = ::hash(k);
	while (ht[h]) {
		if (ht[h] == k)
			return h;
		h = (h + 1) % MaxSize;
	}
	return -1;
}

int main(int argc, char const* argv[]) {
	// for (int i = 0; i < 15; ++i) {
	// 	cout << "insert: " << i << " " << insert(i) << "\n";
	// }
	// 插入0会有个bug
	cout << "insert: " << 1 << " " << (insert(1) ? "True" : "False") << "\n";
	cout << "insert: " << 14 << " " << (insert(14) ? "True" : "False") << "\n";
	cout << "insert: " << 27 << " " << (insert(27) ? "True" : "False") << "\n";
	return 0;
}