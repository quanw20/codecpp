#include <bits/stdc++.h>
using namespace std;
/**
 * @brief 二分查找
 *
 * @param a 数组
 * @param len 长度
 * @param val 待查找的值
 * @return int 下标
 */
int binary_search(int a[], int len, int val) {
	int l = 0, r = len - 1, mid;
	while (l <= r) {
		mid = l + (r - l >> 1);
		if (a[mid] == val)
			return mid;
		else if (a[mid] < val)
			l = mid + 1;
		else if (a[mid] > val)
			r = mid - 1;
	}
	return -1;
}
int binary_search_r(int a[], int l, int r, int val) {
	int mid = l + (r - l >> 1);
	if (a[mid] == val)
		return mid;
	else if (a[mid] < val)
		return binary_search_r(a, mid + 1, r, val);
	else if (a[mid] > val)
		return binary_search_r(a, l, mid - 1, val);
}


int main(int argc, char const* argv[]) {
	int a[] = { 1, 2, 3, 4, 5, 6, 7, 8, 98 };
	cout << binary_search(a, 9, 98) << '\n';
	cout << binary_search_r(a, 0, 9, 98) << '\n';
	return 0;
}