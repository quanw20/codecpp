#include <bits/stdc++.h>
using namespace std;
/*
而 LFU 算法相当于是把数据按照访问频次进行排序，这个需求恐怕没有那么简单，
而且还有一种情况，如果多个数据拥有相同的访问频次，我们就得删除最早插入的那个数据。
也就是说 LFU 算法是淘汰访问频次最低的数据，如果访问频次最低的数据有多条，需要淘汰最旧的数据。
1、不要企图上来就实现算法的所有细节，而应该自顶向下，逐步求精，先写清楚主函数的逻辑框架，然后再一步步实现细节。

2、搞清楚映射关系，如果我们更新了某个key对应的freq，那么就要同步修改KF表和FK表，这样才不会出问题。

3、画图，画图，画图，重要的话说三遍，把逻辑比较复杂的部分用流程图画出来，然后根据图来写代码，可以极大减少出错的概率。
*/
class LFU {
private:
	int size = 3;
	int min_freq;
	unordered_map<int, int> k2f;
	unordered_map<int, list<int>*> f2ks;
	unordered_map<int, int> k2v;
	std::set<int> ks;
	void increase_freq(int key) {
		int freq = k2f.find(key)->second;
		k2f.find(key)->second++;
		f2ks.find(freq)->second->remove(key);
		f2ks.insert_or_assign(freq + 1, new list<int>());
		f2ks.find(freq + 1)->second->push_back(key);
		if (f2ks.find(freq)->second->empty()) {
			f2ks.erase(freq);
			if (freq == min_freq)
				++min_freq;
		}
	}
	void remove_min_freq_key() {
		list<int>* ls = f2ks.find(min_freq)->second;
		int del_key = ls->front();
		ls->remove(del_key);
		if (ls->empty()) {
			f2ks.erase(min_freq);
		}
		k2v.erase(del_key);
		k2f.erase(del_key);
	}


public:
	void put(int key, int val) {
		if (k2v.find(key) != k2v.cend()) { // key存在
			k2v.find(key)->second = val;
			increase_freq(key);
		} else {
			if (k2v.size() == size) {
				// 淘汰容量最小的key
				remove_min_freq_key();
			}
			k2v.insert(pair<int, int>(key, val));
			k2f.insert(pair<int, int>(key, 1));
			f2ks.insert_or_assign(1, new list<int>());
			f2ks.find(1)->second->push_back(key);
			ks.insert(key);
			min_freq = 1;
		}
	}


	int get(int key) {
		if (k2v.find(key) == k2v.cend())
			return -1;
		// f++
		increase_freq(key);

		return k2v.find(key)->second;
	}
};
int main(int argc, char const* argv[]) {

	return 0;
}