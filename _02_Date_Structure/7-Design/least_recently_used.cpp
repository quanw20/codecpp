#include <bits/stdc++.h>
using namespace std;
/*
 * 计算机的缓存容量有限，如果缓存满了就要删除一些内容，给新内容腾位置。
 * 但问题是，删除哪些内容呢？
 * 我们肯定希望删掉哪些没什么用的缓存，而把有用的数据继续留在缓存里，方便之后继续使用。
 * 那么，什么样的数据，我们判定为「有用的」的数据呢？
 * LRU 缓存淘汰算法就是一种常用策略。
 * LRU 的全称是 Least Recently Used，也就是说我们认为最近使用过的数据应该是是「有用的」，
 * 很久都没用过的数据应该是无用的，内存满了就优先删那些很久没用过的数据。
 *
 */
class LRU {
private:
	int size; // 缓存数
	list<int> cache;
	unordered_map<int, int> mp;

public:
	LRU() : size(3) {}
	LRU(int size) : size(size) {}
	void put(int key, int val) {
		if (mp.find(key) != mp.cend()) {
			mp.insert(pair<int, int>(key, val));
			cache.remove(key);
			cache.push_back(key);
		} else {
			if (mp.size() == size) {
				int k = cache.front();
				cache.pop_front();
				mp.erase(mp.find(k));
			}
			mp.insert(pair<int, int>(key, val));
			cache.push_back(key);
		}
	}
	int get(int key) {
		if (mp.find(key) == mp.cend())
			return -1;
		int ret = mp.find(key)->second;
		cache.remove(ret);
		cache.push_back(ret);
		return ret;
	}
};
int main(int argc, char const* argv[]) {
	LRU lru;
	lru.put(1, 2);
	cout << lru.get(1) << '\n';
	cout << lru.get(2) << '\n';
	lru.put(2, 3);
	cout << lru.get(1) << '\n';
	cout << lru.get(2) << '\n';
	lru.put(3, 4);
	cout << lru.get(2) << '\n';
	cout << lru.get(3) << '\n';
	lru.put(4, 5);
	cout << lru.get(1) << '\n';
	cout << lru.get(4) << '\n';
	return 0;
}