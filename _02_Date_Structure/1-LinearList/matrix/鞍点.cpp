#include <bits/stdc++.h>
using namespace std;
void resolve(int a[][3], int n, int m) {
	for (int i = 0; i < n; ++i) {
		int min = INT_MAX, mIdx = -1, max = INT_MIN, MIdx = -1;
		for (int j = 0; i < m; ++j) {
			if (a[i][j] < min) {
				min = a[i][j];
				mIdx = j;
			}
			for (int k = 0; k < n; ++k) {
				if (a[k][mIdx] > max) {
					max = a[k][mIdx];
					MIdx = k;
				}
			}
			if (MIdx == i)
				cout << '(' << i << ", " << j << ")\n";
		}
	}
}
int main(int argc, char const* argv[]) {
	int a[4][5] = {
		{ 1, 2, 3, 4, 5 },
		{ 2, 3, 4, 5, 6 },
		{ 4, 5, 6, 7, 8 },
		{ 5, 6, 7, 8, 9 }
	};
    int b[2][3]={1,2,3,2,3,4};
	resolve(b, 4, 5);
	return 0;
}