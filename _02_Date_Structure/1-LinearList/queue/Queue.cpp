#include "Queue.h"
#include "LinkedQueue.h"
#include <bits/stdc++.h>
using namespace std;
void test_Queue() {
	cout << "#****************循环数组队列测试***************#\n";
	auto q = new Queue<string>(5);
	cout << "\n-----测试入队: A1 A2 A3 A4 A5\n";
	q->enQueue("A1");
	q->enQueue("A2");
	q->enQueue("A3");
	q->enQueue("A4");
	q->enQueue("A5");
	cout << "\n-----测试出队:\n";
	cout << q->deQueue() << " ";
	cout << q->deQueue() << " ";
	cout << q->deQueue() << " ";
	cout << "\n-----队为空? " << q->empty() ? "True\n" : "False\n";
	cout << "\n-----队首元素是: " << q->getHead() << '\n';
	cout << "\n-----测试入队: A6 A7 A8 A9\n";
	q->enQueue("A6");
	q->enQueue("A7");
	q->enQueue("A8");
	q->enQueue("A9");
	cout << "\n-----测试出队:\n";
	cout << q->deQueue() << " ";
	cout << q->deQueue() << " ";
	cout << q->deQueue() << " ";
	cout << "\n-----测试入队:A10 A11 A12\n";
	q->enQueue("A10");
	q->enQueue("A11");
	q->enQueue("A12");
	cout << "\n-----测试出队:\n";
	cout << q->deQueue() << " ";
	cout << q->deQueue() << " ";
	cout << q->deQueue() << " ";
	cout << q->deQueue() << " ";
	cout << q->deQueue() << " ";
	// cout << q->deQueue() << " ";
}
void test_LinkedQueue() {
	cout << "\n#****************链表队列测试***************#\n";
	auto q = new LinkedQueue<string>();
	cout << "\n-----测试入队: A1 A2 A3 A4 A5\n";
	q->enQueue("A1");
	q->enQueue("A2");
	q->enQueue("A3");
	q->enQueue("A4");
	q->enQueue("A5");
	cout << "\n-----测试出队:\n";
	cout << q->deQueue() << " ";
	cout << q->deQueue() << " ";
	cout << q->deQueue() << " ";
	cout << "\n-----队为空? " << q->empty() ? "True\n" : "False\n";
	cout << "\n-----队首元素是: " << q->getHead() << '\n';
	cout << "\n-----测试入队: A6 A7 A8 A9\n";
	q->enQueue("A6");
	q->enQueue("A7");
	q->enQueue("A8");
	q->enQueue("A9");
	cout << "\n-----测试出队:\n";
	cout << q->deQueue() << " ";
	cout << q->deQueue() << " ";
	cout << q->deQueue() << " ";
	cout << "\n-----测试入队:A10 A11 A12\n";
	q->enQueue("A10");
	q->enQueue("A11");
	q->enQueue("A12");
	cout << "\n-----测试出队:\n";
	cout << q->deQueue() << " ";
	cout << q->deQueue() << " ";
	cout << q->deQueue() << " ";
	cout << q->deQueue() << " ";
	cout << q->deQueue() << " ";
	// cout << q->deQueue() << " ";
}
int main(int argc, char const* argv[]) {
	test_Queue();
	test_LinkedQueue();
	return 0;
}
