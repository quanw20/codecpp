#include <bits/stdc++.h>
using namespace std;
const int N = 1e5 + 1;
int q[N], a[N];
int hh, tt, k;
/**
 * @brief 滑动窗口
 * 计算长度为k的窗口内的最值
 *
 *  1. 用普通队列怎么做
 *  2. 将队列中没有用的元素删掉-> 具有了单调性
 *  3. 可以用O(1)的时间复杂度从对头/队尾取元素
 *
 * @param argc
 * @param argv
 * @return int
 */
int main(int argc, char const* argv[]) {
	int n, k;
	cin >> n >> k;
	for (int i = 0; i < n; ++i)
		cin >> a[i];
	hh = 0, tt = -1;
	for (int i = 0; i < n; ++i) { // 队列存存放索引
		// 当前对头不在滑动窗口中
		if (hh <= tt && q[hh] < i - k + 1)
			hh++;
		// 队列不为空,队尾是否大于a[i],是则队尾不会被输出
		while (hh <= tt && a[q[tt]] >= a[i])
			tt--;
		q[++tt] = i; // 把当前队尾加进来
		if (i >= k - 1)
			cout << a[q[hh]] << ' '; // 输出对头 最小值
	}
	puts("");

	hh = 0, tt = -1;
	for (int i = 0; i < n; ++i) {
		if (hh <= tt && q[hh] < i - k + 1)
			++hh;
		while (hh <= tt && a[q[tt]] <= a[i])
			--tt;
		q[++tt] = i;
		if (i >= k - 1)
			cout << a[q[hh]] << ' ';
	}
	puts("");
	return 0;
}
const int n = 1e5 + 1;
bitset<100005> b[100005];
void f() {
	
}