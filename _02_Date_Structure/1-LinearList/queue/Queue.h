#ifndef __QUEUE_H__
#define __QUEUE_H__
/**
 * 顺序队列(基于数组)
 *
 */
template <typename T>
class Queue {
private:
	T* a;
	const static int DEFAULT_SIZE = 16;
	int size;
	int rear, front;

public:
	/* 初始化 */
	Queue();
	/**
	 * @param n 队列长度
	 */
	Queue(int n);
	/* 释放空间 */
	~Queue();
	/**
	 * @param t 入队的元素
	 * @return 空间满了?false:true
	 */
	bool enQueue(T t);
	/* 出队 */
	T deQueue();
	/* 取对头 */
	T getHead();
	/* 判断是否为空 */
	bool empty();
};
template <typename T>
Queue<T>::Queue() {
	a = new T[DEFAULT_SIZE];
	memset(a, 0, DEFAULT_SIZE * sizeof(T));
	size = DEFAULT_SIZE;
	rear = front = 0;
}
template <typename T>
Queue<T>::Queue(int n) {
	a = new T[n + 1];
	memset(a, 0, (n + 1) * sizeof(T));
	size = n + 1;
	rear = front = 0;
}
template <typename T>
Queue<T>::~Queue() {
	delete a;
}
template <typename T>
bool Queue<T>::enQueue(T t) {
	if ((rear + 1) % size != front) {
		rear = (rear + 1) % size;
		a[rear] = t;
		return true;
	}
	return false;
}
template <typename T>
T Queue<T>::deQueue() {
	if (rear != front) {
		front = (front + 1) % size;
		return a[front];
	}
	return nullptr;
}
template <typename T>
T Queue<T>::getHead() {
	if (rear != front)
		return a[front];
	return nullptr;
}
template <typename T>
bool Queue<T>::empty() {
	return rear == front ? true : false;
}

#endif