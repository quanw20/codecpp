#ifndef __LINKED_QUEUE_H__
#define __LINKED_QUEUE_H__
template <typename T>
class Node {
public:
	T val;
	Node<T>* next;
	Node(T t) : val(t) {}
};
template <typename T>
class LinkedQueue {
private:
	Node<T>*head, *tail;

public:
	/* 初始化 */
	LinkedQueue();
	/* 析构 */
	~LinkedQueue();
	/* 入队 */
	bool enQueue(T t);
	/* 出队 */
	T deQueue();
	/* 获取头元素 */
	T getHead();
	/* 判空 */
	bool empty();
};
template <typename T>
LinkedQueue<T>::LinkedQueue() {
	head = nullptr;
	tail = nullptr;
}
template <typename T>
LinkedQueue<T>::~LinkedQueue() {
	while (head != nullptr) {
		Node<T>* p = head;
		head = head->next;
		delete p;
	}
	tail = nullptr;
}
template <typename T>
bool LinkedQueue<T>::enQueue(T t) {
	Node<T>* node = new Node<T>(t);
	if (head == nullptr) {
		head = node;
		tail = node;
		return true;
	}
	tail->next = node;
	tail = node;
	return true;
}
template <typename T>
T LinkedQueue<T>::deQueue() {
	if (head != nullptr) {
		Node<T>* node = head;
		T val = head->val;
		head = head->next;
		delete node;
		return val;
	}
	return nullptr;
}
template <typename T>
T LinkedQueue<T>::getHead() {
	return head->val;
}
template <typename T>
bool LinkedQueue<T>::empty() {
	return head == nullptr;
}
#endif