#include <bits/stdc++.h>
using namespace std;
template <class T>
class Queue {
private:
	T* queue;
	int flag, front, rear, len;

public:
	Queue() {
		len = 3;
		queue = new T[len];
		flag = front = rear = 0;
	}
	void push(const T v) {
		if (flag == 1 && front == rear)
			return;
		flag = 1;
		queue[rear] = v;
		rear = (rear + 1) % len;
	}
	T pop() {
		if (flag == 0 && front == rear)
			return 0;
		int r = queue[front];
		front = (front + 1) % len;
		if (front == rear)
			flag = 0;
		return r;
	}
};
int main(int argc, char const* argv[]) {
	Queue<int> q;
	q.push(1);
	q.push(2);
	q.push(3);
	q.push(4);
	cout << q.pop() << ' ';
	cout << q.pop() << ' ';
	cout << q.pop() << ' ';

	return 0;
}