#include <bits/stdc++.h>
using namespace std;

template <typename T>
class Node {
public:
	T val;
	Node<T>* next;
	Node(T t) : val(t) {}
};
template <typename T>
class Queue {
private:
	Node<T>* tail = nullptr;

public:
	void push(const T& v) {
		auto p = new Node<T>(v);
		if (tail == nullptr) {
			tail = p;
			p->next = p;
		} else {
			auto nxt = tail->next;
			tail->next = p;
			p->next = nxt;
			tail = p;
		}
	}
	T pop() {
		if (tail == nullptr)
			return 0;
		auto p = tail->next;
		T ret = p->val;
		if (p == tail)
			tail = nullptr;
		else
			tail->next = p->next;
		delete p;
		return ret;
	}
};

int main(int argc, char const* argv[]) {
	Queue<int> q;
	q.push(1);
	q.push(2);
	q.push(3);
	q.push(4);
	q.pop();
	q.pop();
	q.pop();
	q.pop();
	return 0;
}