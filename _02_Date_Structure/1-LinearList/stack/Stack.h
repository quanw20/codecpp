#ifndef __STACH_H__
#define __STACK_H__
/**
 * 使用数组实现的Stack
 *
 * a = new T[n];
 *
 * a[++p]=t
 *
 * return a[p--]
 *
 * p?-1
 */
template <typename T>
class Stack {
private:
	T* a;
	unsigned size;
	int p;
	const static unsigned DEFAULE_SIZE = 16;

public:
	/* 初始化 */
	Stack();
	/* 初始化 */
	Stack(unsigned n);
	/* 析构释放数组 */
	~Stack();
	/* 入栈 */
	bool push(T t);
	/* 出栈 */
	T pop();
	/* 取栈顶 */
	T top();
	/* 判空 */
	bool empty();
};
template <typename T>
Stack<T>::Stack() {
	size = DEFAULE_SIZE;
	a = new T[DEFAULE_SIZE];
	memset(a, 0, DEFAULE_SIZE * sizeof(T));
	p = -1;
}
template <typename T>
Stack<T>::Stack(unsigned n) {
	size = n;
	a = new T[n];
	memset(a, 0, n * sizeof(T));
	p = 0;
}
template <typename T>
Stack<T>::~Stack() {
	delete[] a;
	a = nullptr;
	p = size = -1;
}
template <typename T>
bool Stack<T>::push(T t) {
	if (p < size - 1) {
		a[++p] = t;
		return true;
	}
	return false;
}
template <typename T>
T Stack<T>::pop() {
	if (p == -1)
		return nullptr;
	return a[p--];
}
template <typename T>
T Stack<T>::top() {
	if (p != -1)
		return a[p];
	return nullptr;
}
template <typename T>
bool Stack<T>::empty() {
	return p == -1;
}


#endif