#include <bits/stdc++.h>
#include "LinkedStack.h"
using namespace std;
/**
 * 反转
 */
template <class T>
void reverse(const T* a, int n) {
	LinkedStack<T>* s = new LinkedStack<T>();
	for (int i = 0; i < n; ++i) {
		s->push(a[i]);
	}
	while (!s->empty()) {
		std::cout << s->pop() << ' ';
	}
	cout << endl;
	delete s;
}
/**
 * 进制转换
 * conversion of number systems
 *
 * 10 -> 2
 */
void dec2bin(int n) {
	LinkedStack<int> s;
	while (n != 0) {
		s.push(n % 2);
		n /= 2;
	}
	while (!s.empty()) {
		cout << s.pop() << ' ';
	}
	cout << endl;
}
void dec2bin(double n, int e = 10) {
	int z = (int)n;
	double d = n - z;
	LinkedStack<int> s;
	while (z != 0) {
		s.push(z % 2);
		z /= 2;
	}
	while (!s.empty()) {
		cout << s.pop();
	}
	cout << '.';
	for (int i = 0; i < e; ++i) {
		if (d == 0)
			break;
		d *= 2;
		if (d >= 1) {
			cout << 1;
			--d;
		}
		else {
			cout << 0;
		}
	}
}
/**
 * 进制转换
 * conversion of number systems
 *
 * 10 -> 8
 */
void dec2oct(int n) {
	LinkedStack<int> s;
	while (n != 0) {
		s.push(n % 8);
		n /= 8;
	}
	while (!s.empty()) {
		cout << s.pop() << ' ';
	}
	cout << endl;
}

/**
 * 进制转换
 * conversion of number systems
 *
 * 10 -> 16
 */
void dec2hex(int n) {
	LinkedStack<char> s;
	while (n != 0) {
		if (n % 16 > 9) {
			s.push('A' + (n % 16) - 10);
		}
		else
			s.push('0' + n % 16);
		n /= 16;
	}
	while (!s.empty()) {
		cout << s.pop() << ' ';
	}
	cout << endl;
}
/**
 * 括号匹配
 */
bool bracketMatching(const char* a, int n) {
	LinkedStack<char> s;
	for (int i = 0; i < n; ++i) {
		if (a[i] == '(')
			s.push(a[i]);
		if (a[i] == ')') {
			if (!s.empty())
				s.pop();
			else
				return false;
		}
		if (a[i] == '[')
			s.push(a[i]);
		if (a[i] == ']') {
			if (!s.empty())
				s.pop();
			else
				return false;
		}
		if (a[i] == '{')
			s.push(a[i]);
		if (a[i] == '}') {
			if (!s.empty())
				s.pop();
			else
				return false;
		}
	}
	if (!s.empty())return false;
	return true;
}

int main(int argc, char const* argv[]) {
	char a[] = { 'A', 'B', 'C', 'D', 'E', 'F' };
	cout << "----------\n反转数组 {'A','B','C','D','E','F'} \n";
	reverse(a, sizeof(a) / sizeof(a[0]));
	cout << "----------\n17D 转二进制:\n";
	dec2bin(17);
	cout << "----------\n17D 转八进制:\n";
	dec2oct(17);
	cout << "----------\n17D 转十六进制:\n";
	dec2hex(17);
	cout << "----------\n括号匹配 '(a)af((s()fa()())' :\n";
	char s[] = "(a)af((s()fa()())";
	cout << (bracketMatching(s, sizeof(s) / sizeof(s[0])) ? "True\n" : "False\n");
	cout << "----------\n括号匹配 '(a(()d)(s))()' :\n";
	char f[] = "(a(()d)(s))()";
	cout << (bracketMatching(f, sizeof(f) / sizeof(f[0])) ? "True\n" : "False\n");
	cout << "----------\n17.256D 转二进制:\n";
	dec2bin(17.256);
	return 0;
}
