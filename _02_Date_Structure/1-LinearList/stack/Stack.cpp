#include "Stack.h"
#include "LinkedStack.h"
#include <bits/stdc++.h>
using namespace std;
int main(int argc, char const* argv[]) {
	Stack<string>* s = new Stack<string>(8);
	s->push("ASD");
	s->push("FGH");
	s->push("JKL");
	s->push("JKL");
	s->push("JKL");
	s->push("JKL");
	s->push("JKL");
	s->push("JKL");
	while (!s->empty())
		cout << s->pop() << " ";
	cout << "\n";
	LinkedStack<string>* ls = new LinkedStack<string>();
	ls->push("Tang");
	ls->push("Tang1");
	ls->push("Tang2");
	ls->push("Tang3");
	ls->push("Tang4");
	ls->push("Tang5");
	ls->push("Tang6");
	ls->push("Tang7");
	ls->push("Tang8");
	ls->push("Tang9");

	return 0;
}
