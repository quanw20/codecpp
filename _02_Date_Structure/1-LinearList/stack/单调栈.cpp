#include <bits/stdc++.h>
using namespace std;
// 单调栈
// bf -> 去掉不用的
// 左边最小的数,没有返回-1
/**
 *
 *
 *
 * eg.
 *  3,4, 2,7,5
 * -1,3,-1,2,2
 */
void bf() {
	int a[] = { 3, 4, 2, 7, 5 };
	int b[5]; // i左边比a[i]小的第一个数,没有返回-1
	for (int i = 0; i < 5; i++) {
		int j = i - 1;
		for (; j >= 0; --j) {
			if (a[j] < a[i]) {
				b[i] = a[j];
				break;
			}
		}
		if (j == -1)
			b[i] = -1;
	}
	for (int i = 0; i < 5; ++i) {
		cout << b[i] << ' ';
	}
}
void monotonous_stack() {
	int s[10001];
	int idx = 0;
	s[0] = INT_MAX;
	int a[] = { 3, 4, 2, 7, 5 };
	int b[5]; // i左边比a[i]小的第一个数,没有返回-1
	for (int i = 0; i < 5; ++i) {
		if (idx > 0 && s[idx] < a[i])
			b[i] = s[idx];
		else
			b[i] = -1;
		if (idx >= 0 && a[i] < s[idx])
			s[++idx] = a[i];
	}
	for (int i = 0; i < 5; ++i) {
		cout << b[i] << ' ';
	}
	cout << '\n';
	for (int i = 0; i <= idx; ++i) {
		cout << s[i] << ' ';
	}
}
int stk[1001]; //{0}
int tt;		   //=0
int main(int argc, char const* argv[]) {
	// ifstream cin("a.txt");
	int n;
	cin >> n;
	while (n--) {
		int x;
		cin >> x;
		// 栈不为空且不会再用到当前元素
		while (tt && stk[tt] >= x)
			--tt;
		if (tt) // 栈不为空
			cout << stk[tt] << ' ';
		else
			cout << -1 << ' ';
		// 入栈
		stk[++tt] = x;
	}

	return 0;
}