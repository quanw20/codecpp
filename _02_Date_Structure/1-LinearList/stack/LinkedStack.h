#ifndef __LINKEDSTACK_H__
#define __LINKEDSTACK_H__
template <typename T>
class LSNode {
public:
	T val;
	LSNode<T>* next;
	LSNode(T t) : val(t) {}
};

template <typename T>
class LinkedStack {
private:
	LSNode<T>* __top;

public:
	/**
	 * 初始化
	 */
	LinkedStack();
	/**
	 * 析构
	 */
	~LinkedStack();
	/**
	 * 入栈
	 */
	bool push(T t);
	/**
	 * 弹出
	 */
	T pop();
	/**
	 * 取栈顶元素
	 */
	T top();
	/**
	 * 判空
	 */
	bool empty();
};
template <typename T>
LinkedStack<T>::LinkedStack() : __top(nullptr) {}
template <typename T>
LinkedStack<T>::~LinkedStack() {
	while (__top != nullptr) {
		LSNode<T>* node = __top;
		__top = __top->next;
		delete node;
	}
}
template <typename T>
bool LinkedStack<T>::push(T t) {
	LSNode<T>* node = new LSNode<T>(t);
	node->next = __top;
	__top = node;
	return true;
}
template <typename T>
T LinkedStack<T>::pop() {
	LSNode<T>* node = __top;
	T val = __top->val;
	__top = __top->next;
	delete node;
	return val;
}
template <typename T>
T LinkedStack<T>::top() {
	return __top->val;
}
template <typename T>
bool LinkedStack<T>::empty() {
	return __top == nullptr;
}
#endif