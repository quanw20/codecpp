#include <bits/stdc++.h>
using namespace std;
class Node {
public:
	int val;
	Node* next;
	Node(int v) : val(v), next(nullptr) {}
};

int main() {
	register int m = 40, n = 3;
	Node *head = new Node(1), *p = head, *pre;

	for (int i = 2; i <= m; i++) {
		p->next = new Node(i);
		p = p->next;
	}
	p->next = head;
	pre = p;
	p = p->next;
	for (int i = 1; p != pre; i++) {
		if (i == n) {
			i = 0;
			cout << p->val << " ";
			Node* n = p;
			pre->next = p->next;
			pre = p;
			p = p->next;
			// delete n;
			continue;
		}
		pre = p;
		p = p->next;
	}
	return 0;
}