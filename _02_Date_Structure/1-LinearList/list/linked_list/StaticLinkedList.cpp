#include <bits/stdc++.h>
using namespace std;

const int SIZE = 1e5 + 1;
// typedef int NodeType;
// struct SNode {
// 	NodeType val; // 节点值
// 	int next;	  // 指向下一个节点的指针
// } sl[SIZE];		  // 这就是静态链表了

template <class T>
struct SNode {
	T val;	  // 节点值
	int next; // 指向下一个节点的指针
};
SNode<int> sl[SIZE];
int dupl[SIZE];

int main(int argc, char const* argv[]) {
	// ifstream cin("D:\\workspaceFolder\\CODE_CPP\\a.txt");
	memset(dupl, 0, sizeof dupl);
	int head, n, addr, null = -1;
	cin >> head >> n;
	while (n--)
		cin >> addr >> sl[addr].val >> sl[addr].next;

	int head2 = null, pre2 = 0, pre = head;
	for (int i = head; i != null; i = sl[i].next) { // 去重
		if (!dupl[abs(sl[i].val)]) {
			dupl[abs(sl[i].val)] = 1;
			pre = i;
		} else {
			sl[pre].next = sl[i].next; // 从sl1上解下来
			if (head2 == null)		   // 连到sl2上
				head2 = i;
			else
				sl[pre2].next = i;
			pre2 = i;
		}
	}
	sl[pre2].next = null; // 结尾

	for (int i = head; i != null; i = sl[i].next) { // 输出
		if (sl[i].next != null)
			printf("%05d %d %05d\n", i, sl[i], sl[i].next);
		else
			printf("%05d %d %d\n", i, sl[i], sl[i].next);
	}
	for (int i = head2; i != null; i = sl[i].next)
		if (sl[i].next != null)
			printf("%05d %d %05d\n", i, sl[i], sl[i].next);
		else
			printf("%05d %d %d\n", i, sl[i], sl[i].next);
	return 0;
}