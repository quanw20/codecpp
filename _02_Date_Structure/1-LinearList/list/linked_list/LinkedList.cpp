#include "LinkedList.h"
using namespace std;
LinkedList<string>* ll;
void init() {
	cout << "----\n初始化\n";
	ll = new LinkedList<string>();
	ll->insertHead("Quanwei");
	ll->insertHead("Tang");
	ll->insertHead("Hi");
	ll->insertTail("Hello");
	ll->insertTail("Linked");
	ll->insertTail("List");
	ll->insertTail("Node");
	cout << "init: " << *ll << "\n";
}
void testInsert() {
	cout << "----\n插入测试:\n";
	ll->insert(0, "芜湖~");
	cout << "insert: \n"
		 << *ll << "\n";
	ll->insert(0, "芜湖~");
	cout << "insert: \n"
		 << *ll << "\n";
	ll->insert(ll->size(), "尾巴前面一个~");
	cout << "insert: \n"
		 << *ll << "\n";
	ll->insert(ll->size() + 1, "尾巴后面一个~");
	cout << "insert: \n"
		 << *ll << "\n";
	ll->insert(100, "ll->insert(100, \"\");");
	cout << "insert: \n"
		 << *ll << "\n";
	ll->insert(-100, "ll->insert(-100, \"\");");
	cout << "insert: \n"
		 << *ll << "\n";
}
void testIndexOf() {
	cout << "----\n测试获取元素下标: \n";
	cout << "ll->indexOf(\"ll->insert(-100, \"\");\")\t" << ll->indexOf("ll->insert(-100, \"\");") << '\n';
	cout << "ll->indexOf(\"芜湖~\")\t" << ll->indexOf("芜湖~") << '\n';
}
void testAt() {
	cout << "----\n测试通过下标获取: \n";
	cout << "at(0): " << ll->at(0) << '\n';
	cout << "at(size-1): " << ll->at(ll->size() - 1) << '\n';
}
void testReverse() {
	cout << "----\n测试数组反转: \n";
	ll->reverse();
	cout << "全部反转后: \n"
		 << *ll << '\n';
	ll->reverse(5);
	cout << "反转 [0, 5] 后: \n"
		 << *ll << '\n';
	ll->reverse(2, ll->size() - 2);
	cout << "反转 [2, ll->size()-2] 后: \n"
		 << *ll << '\n';
}
void testRemove() {
	cout << "----\n测试移除元素: \n";
	cout << "removeHead: " << ll->removeHead() << '\n';
	cout << "removeTail: " << ll->removeTail() << '\n';
	cout << "remove(\" Tang \"): " << (ll->remove("Tang") ? "true" : "false") << '\n';
	cout << "remove(\" Tang \"): " << (ll->remove("Tang") ? "true" : "false") << '\n';
	cout << "移除后: " << *ll << '\n';
}
void testException() {
	try {
		cout << "at(-1): ";
		cout << ll->at(-1) << '\n';
	}
	catch (const char* e) {
		std::cerr << e << '\n';
	}
}
int main(int argc, char const* argv[]) {
	init();
	testInsert();
	testIndexOf();
	testAt();
	testReverse();
	testRemove();
	testException();
	return 0;
}
