#include <iostream>
#include "ArrayList.h"

using namespace std;

ArrayList<string>* al = nullptr;
ArrayList<string> al1;

void init() {
	al = new ArrayList<string>();
	cout << al->empty() << "\n";
	al->add("Hello");
	al->add("Tang");
	al->add("Quanwei");
	al->add(".");
	al->add("How's");
	al->add("everything");
	al->add("going");
	al->add("?");
	al->add(" :) ");
}
void testEquals() {
	al1.add("Hello");
	al1.add("Tang");
	al1.add("Quanwei");
	al1.add(".");
	al1.add("How's");
	al1.add("everything");
	al1.add("going");
	al1.add("?");
	al1.add(" :) ");
	cout << "al==al1 " << al->equals(al1) << "\n";
	al1.remove(1);
	cout << "al==al1 " << al->equals(al1) << "\n";
}
int main() {
	init();
	testEquals();
	cout << "forEach:\n";
	auto func = [](string x) -> void { cout << x << " "; };
	al->forEach(func);
	cout << "\n";
	cout << al << "\n";
	al->insert(0, "Hi");
	cout << al << "\n";
	al->insert(al->size() - 1, "Hi");
	cout << al << "\n";
	al->insert(al->size(), "Hi");
	cout << al << "\n";
	cout << al->indexOf("Hi") << "\n";
	cout << al->indexOf("Quanwei") << "\n";
	cout << al->lastIndexOf("Quanwei") << "\n";
	cout << al->lastIndexOf("Quanwe") << "\n";
	cout << al << "\n";
	try {
		cout << al->at(3) << "\n";
		cout << al->at(100) << "\n";
	}
	catch (const char* e) {
		cerr << e << '\n';
	}
	try {
		cout << "remove: " << al->remove(1) << '\n';
		cout << "remove: " << al->remove(2) << '\n';
		cout << "remove: " << al->remove(3) << '\n';
		cout << "remove: " << al->remove(103) << '\n';
	}
	catch (const char* e) {
		cerr << e << '\n';
	}
	cout << al << "\n";
	al->~ArrayList();
	cout << al << "\n";

	auto al1 = new ArrayList<int>();
	cout << al1;
	cout << "\n";
	cout << al << "\n";	
	al->reverse();
	cout << al << "\n";


	return 0;
}
