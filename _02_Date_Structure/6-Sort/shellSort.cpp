#include <bits/stdc++.h>
using namespace std;
void shellSort(int* a, int n) {
	for (int d = n / 2; d >= 1; d /= 2) {
		for (int i = d; i < n; ++i) {
			int t = a[i], j = i - d;
			for (; j >= 0 && t < a[j]; j -= d)
				a[j + d] = a[j];
			a[j + d] = t;
		}
	}
}
int main(int argc, char const* argv[]) {
	int a[] = { 9, 8, 7, 6, 5, 4, 3, 2, 1 };
	shellSort(a, 9);
	for_each(a, a + 9, [](int x) { cout << x << " "; });
	return 0;
}