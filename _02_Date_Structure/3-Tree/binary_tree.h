#ifndef __BINARY_TREE_H_
#define __BINARY_TREE_H_
#include <bits/stdc++.h>
using namespace std;
template <class T> // 前置申明
struct TreeNode;
template <class T> // 重定义
using Tree = TreeNode<T>*;
/**
 * @brief 二叉树结点
 * @tparam T 需要重载<=>运算符
 */
template <class T>
struct TreeNode {
	T val;
	Tree<T> left, right;
	TreeNode(T val) : val(val), left(nullptr), right(nullptr) {}
};
/**
 * @brief 二叉树
 * @tparam T 需要重载<=>运算符
 */
template <class T>
class BinaryTree {
private:
	Tree<T> root;
	Tree<T> insert(Tree<T> root, int val);
	Tree<T> search(Tree<T> root, int val);
	Tree<T> create(std::istream& in);
	Tree<T> create(std::ifstream& in);
	void preOrder(Tree<T> root, std::function<void(Tree<T>)> func);
	int height(Tree<T> root) { return root ? max(height(root->left), height(root->right)) + 1 : 0; }
	int countLeaves(Tree<T> root);
	int countNodes(Tree<T> root);
	T maxVal(Tree<T> root);
	bool remove(Tree<T> parent, Tree<T> node, T val);

public:
	/**
	 * @brief 初始化
	 */
	BinaryTree() : root(nullptr) {}
	/**
	 * @brief 从标准输入建树
	 * @param in 标准输入
	 */
	BinaryTree(std::istream& in) { root = create(in); }
	/**
	 * @brief 从文件输入建树
	 * @param in 文件输入
	 */
	BinaryTree(std::ifstream& in) { root = create(in); }
	/**
	 * @brief 插入
	 * @param val 待插值
	 */
	void insert(int val) { root = insert(root, val); }
	/**
	 * @brief 二分搜索O(log(n))
	 * @param val 待搜值
	 * @return Tree<T> 包含值的节点
	 */
	Tree<T> search(int val) { return search(root, val); }
	/**
	 * @brief 获取根节点
	 * @return Tree<T> 根节点
	 */
	Tree<T> getRoot() { return root; }
	/**
	 * @brief 获取叶子节点的个数
	 * @return int 叶子节点的个数
	 */
	int countLeaves() { return countLeaves(root); }
	/**
	 * @brief 获取所有节点的个数
	 * @return int 所有节点的个数
	 */
	int countNodes() { return countNodes(root); }
	/**
	 * @brief (递归)前序遍历
	 * @param func 对每个节点值执行的操作
	 */
	void preOrder(std::function<void(Tree<T>)> func) { preOrder(root, func); }
	/**
	 * @brief (非递归)中序遍历
	 */
	void inOrder();
	/**
	 * @brief (非递归)后序遍历
	 */
	void postOrder();
	/**
	 * @brief 层序遍历
	 */
	void levelOrder();
	/**
	 * @brief (递归)获取高度
	 * @return int 高度
	 */
	int height() { return height(root); }
	/**
	 * @brief 获取该二叉树的最大值
	 * @return T 最大值
	 */
	T maxVal() { return maxVal(root); }
	/**
	 * @brief 移除包含给定值的节点
	 * @param val 待删值
	 * @return true 值存在, 删除成功
	 * @return false 值不存在, 删除失败
	 */
	bool remove(T val) { return remove(root, root, val); }
	/**
	 * @brief (静态函数)判断给定节点是否是二叉搜索树
	 * @param root 根节点
	 * @return true
	 * @return false
	 */
	static bool isBST(Tree<T> root);
};
template <class T>
Tree<T> BinaryTree<T>::insert(Tree<T> root, int val) {
	if (!root)
		root = new TreeNode<T>(val);
	if (val > root->val)
		root->right = insert(root->right, val);
	if (val < root->val)
		root->left = insert(root->left, val);
	return root;
}
template <class T>
Tree<T> BinaryTree<T>::create(std::istream& in) {
	T ch;
	in >> ch;
	if (ch == '#')
		return nullptr;
	Tree<T> bt = new TreeNode<T>(ch);
	bt->left = create(in);
	bt->right = create(in);
	return bt;
}
template <class T>
Tree<T> BinaryTree<T>::create(std::ifstream& in) {
	T ch;
	in >> ch;
	if (ch == '#')
		return nullptr;
	Tree<T> bt = new TreeNode<T>(ch);
	bt->left = create(in);
	bt->right = create(in);
	return bt;
}
template <class T>
Tree<T> BinaryTree<T>::search(Tree<T> root, int val) {
	if (root == nullptr || root->val == val)
		return root;
	if (val > root->val)
		return search(root->right, val);
	return search(root->left, val);
}
template <class T>
void BinaryTree<T>::preOrder(Tree<T> root, std::function<void(Tree<T>)> func) {
	if (root == nullptr)
		return;
	func(root);
	preOrder(root->left, func);
	preOrder(root->right, func);
}
template <class T>
void BinaryTree<T>::inOrder() {
	if (!root)
		return;
	stack<Tree<T>> s;
	Tree<T> bt = root;
	while (bt || !s.empty()) {
		while (bt) {
			s.push(bt);
			bt = bt->left;
		}
		if (!s.empty()) {
			bt = s.top();
			s.pop();
			cout << bt->val << ' ';
			bt = bt->right;
		}
	}
	cout << '\n';
}
template <class T>
void BinaryTree<T>::postOrder() {
	Tree<T> bt = root;
	stack<pair<Tree<T>, int>> s;
	while (bt || !s.empty()) {
		while (bt) {
			s.push(pair<Tree<T>, int>(bt, 0));
			bt = bt->left;
		}
		while (!s.empty() && s.top().second) {
			bt = s.top().first;
			s.pop();
			cout << bt->val << ' ';
			bt = nullptr; // 必要否则无限循环
		}
		if (!s.empty()) {
			auto p = s.top();
			s.pop();
			p.second = 1;
			s.push(p);
			bt = p.first->right;
		}
	}
	cout << '\n';
}
template <class T>
void BinaryTree<T>::levelOrder() {
	if (!root)
		return;
	queue<Tree<T>> q;
	q.push(root);
	while (!q.empty()) {
		Tree<T> temp = q.front();
		q.pop();
		cout << temp->val << " ";
		if (temp->left)
			q.push(temp->left);
		if (temp->right)
			q.push(temp->right);
	}
	cout << '\n';
}
template <class T>
int BinaryTree<T>::countLeaves(Tree<T> root) {
	if (!root)
		return 0;
	if (!root->left && !root->right)
		return 1;
	return countLeaves(root->left) + countLeaves(root->right);
}
template <class T>
int BinaryTree<T>::countNodes(Tree<T> root) {
	if (!root)
		return 0;
	return countNodes(root->left) + countNodes(root->right) + 1;
}
template <class T>
T BinaryTree<T>::maxVal(Tree<T> root) {
	if (!root)
		return INT_MIN;
	if (!root->right)
		return root->val;
	return maxVal(root->right);
}
template <class T>
bool BinaryTree<T>::remove(Tree<T> parent, Tree<T> node, T val) {
	if (!node)
		return false;
	if (node->val == val) {				 // 找到了
		if (node->left && node->right) { // 左右都不为空
			T v = maxVal(node->left);	 // 用左边最大值or右边最小值填充
			remove(root, root, v);
			node->val = v;
		} else if (!node->left || !node->right) { // 左右有一个为空或者两个都为空
			Tree<T> maybe_nonnull = node->left ? node->left : node->right;
			if (node == root) {
				delete root;
				root = maybe_nonnull;
			} else if (val < parent->val) { // node在左边
				delete parent->left;
				parent->left = maybe_nonnull;
			} else { // node在右边
				delete parent->right;
				parent->right = maybe_nonnull;
			}
		}
		return true;
	} else if (val < node->val)
		return remove(node, node->left, val);
	else
		return remove(node, node->right, val);
}
template <class T>
bool BinaryTree<T>::isBST(Tree<T> root) {
	Tree<T> prev = nullptr;
	if (root) {
		if (!isBST(root->left))
			return false;
		if (prev != nullptr &&
			root->val <= prev->val)
			return false;
		prev = root;
		return isBST(root->right);
	}
	return true;
}
#endif