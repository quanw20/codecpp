#include "binary_tree.h"
BinaryTree<int> root;
void init() {
	cout << "\n<>- init -<>\n";
	cout << "insert 5 3 1 2 4 7 6 9 8 \n";
	root.insert(5);
	root.insert(3);
	root.insert(1);
	root.insert(2);
	root.insert(4);
	root.insert(7);
	root.insert(6);
	root.insert(9);
	root.insert(8);
}
void test_properities() {
	cout << "\n<>- 测试相关属性 -<>\n";
	cout << "根节点高度: " << root.height() << '\n';
	cout << "节点数: " << root.countNodes() << '\n';
	cout << "叶子数: " << root.countLeaves() << '\n';
	cout << "是否是二叉搜索树: " << (BinaryTree<int>::isBST(root.getRoot()) ? "True" : "False") << '\n';
}
void test_traversal() {
	cout << "\n<>- 测试遍历 -<>\n";
	cout << "(递归)前序遍历\n";
	root.preOrder([](auto x) { cout << x->val << " "; });
	cout << "\n(非递归)中序遍历\n";
	root.inOrder();
	cout << "(非递归递归)后序遍历\n";
	root.postOrder();
	cout << "(非递归)层序序遍历\n";
	root.levelOrder();
}
void test_remove() {
	cout << "\n<>- 测试移除 -<>\n";
	cout << "移除值为5的节点:" << '\n';
	root.remove(5);
	test_traversal();
}
void test_istreaminit() {
	cout << "\n<>- 测试列流初始化 -<>\n";
	ifstream cin("nodes.txt");
	BinaryTree<char> tree(cin);
	cout << "(递归)前序遍历\n";
	tree.preOrder([](auto x) { cout << x->val << " "; });
	cout << "\n(非递归)中序遍历\n";
	tree.inOrder();
	cout << "(非递归递归)后序遍历\n";
	tree.postOrder();
	cout << "(非递归)层序序遍历\n";
	tree.levelOrder();
}
void test_preorder_with_func() {
	cout << "\n<>- 测试有回调函数的前序遍历 -<>\n";
	cout << "1. 递归交换左右子树\n";
	root.preOrder([](auto root) {
		auto temp = root->left;
		root->left = root->right;
		root->right = temp;
	});
	test_traversal();
	cout << "2. 前序打印叶子节点\n";
	root.preOrder([](auto x) { 
        if(!x->left&&!x->right)
        cout << x->val << " "; });
}
int main(int argc, char const* argv[]) {
	init();
	test_traversal();
	test_properities();
	test_remove();
	test_preorder_with_func();
	test_istreaminit();
	return 0;
}
