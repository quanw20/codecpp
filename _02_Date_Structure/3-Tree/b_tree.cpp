#include <iostream>
using namespace std;
/**
 * @brief B树节点
 *
 */
class BTreeNode {
	int* keys;	   // 键数组
	int t;		   // 最小度（键的范围）
	BTreeNode** C; // 子节点ptr数组
	int n;		   // 当前节点的键数
	bool leaf;	   // 是否是叶节点
public:
	/**
	 * @brief B树节点
	 *
	 * @param _t 最小度
	 * @param _leaf 是否是节点
	 */
	BTreeNode(int _t, bool _leaf);


	/**
	 * @brief 插入新节点到子树，节点没满时调用
	 *
	 * @param k 键值
	 */
	void insertNonFull(int k);

	/**
	 * @brief 分裂y子树(y子树必须是满的)
	 *
	 * @param i 子树y在子数组C[] 中的下标
	 * @param y 当前节点的一个孩子
	 */
	void splitChild(int i, BTreeNode* y);

	/**
	 * @brief 中序遍历
	 *
	 */
	void traverse();

	/**
	 * @brief 搜索（当前节点为根）
	 *
	 * @param k 键值
	 * @return BTreeNode* NULL键不存在
	 */
	BTreeNode* search(int k);

	friend class BTree; //让BTree类可以访问私有属性
};

// A BTree
class BTree {
	BTreeNode* root; // 根
	int t;			 // 最小度
public:
	BTree(int _t) : root(NULL), t(_t) {}

	void traverse() {
		if (root != NULL)
			root->traverse();
	}
	BTreeNode* search(int k) { return (root == NULL) ? NULL : root->search(k); }

	void insert(int k);
};

BTreeNode::BTreeNode(int t, bool leaf) : t(t), leaf(leaf), n(0) {
	keys = new int[2 * t - 1];
	C = new BTreeNode*[2 * t];
}

void BTreeNode::traverse() {
	// 有n个键[0,n-1],n+1个孩子[0,n]
	int i = 0;
	for (; i < n; ++i) {
		if (leaf == false)
			C[i]->traverse();	// 左
		cout << " " << keys[i]; // 中
	}
	if (leaf == false)
		C[i]->traverse(); // 右
}

BTreeNode* BTreeNode::search(int k) {
	int i = 0; // 找到第一个>=k的元素
	for (; i < n && k > keys[i]; ++i)
		;
	if (keys[i] == k) // 找到了
		return this;
	return leaf ? NULL : C[i]->search(k); // 有子节点就搜索子节点
}

void BTree::insert(int k) {
	if (root == NULL) { // 树为空的情况
		root = new BTreeNode(t, true);
		root->keys[0] = k;						// 插入键
		root->n = 1;							// 更新当前节点的键数
	} else if (root->n != 2 * t - 1) {			// 树不为空且根节点没满
		root->insertNonFull(k);					// 直接插入
	} else {									// 树不为空且根节点满了-> 增加高度
		BTreeNode* s = new BTreeNode(t, false); // 新节点
		s->C[0] = root;							// 将原来的根节点变成新节点的孩子
		s->splitChild(0, root);					// 分裂原来的根节点
		int i = 0;								// 新节点现在有两个孩子,决定用那个节点放k
		if (s->keys[0] < k)
			i++;
		s->C[i]->insertNonFull(k);
		root = s; // 将新节点变成根
	}
}

void BTreeNode::insertNonFull(int k) {
	int i = n - 1;		// Initialize index as index of rightmost element
	if (leaf == true) { // If this is a leaf node
		// The following loop does two things
		// a) Finds the location of new key to be inserted
		// b) Moves all greater keys to one place ahead
		while (i >= 0 && keys[i] > k) {
			keys[i + 1] = keys[i];
			i--;
		}
		keys[i + 1] = k; // Insert the new key at found location
		++n;			 // 键数++
	} else {			 // If this node is not leaf
		// Find the child which is going to have the new key
		while (i >= 0 && keys[i] > k)
			i--;

		// See if the found child is full
		if (C[i + 1]->n == 2 * t - 1) {
			// If the child is full, then split it
			splitChild(i + 1, C[i + 1]);

			// After split, the middle key of C[i] goes up and
			// C[i] is splitted into two. See which of the two
			// is going to have the new key
			if (keys[i + 1] < k)
				i++;
		}
		C[i + 1]->insertNonFull(k);
	}
}

// A utility function to split the child y of this node
// Note that y must be full when this function is called
void BTreeNode::splitChild(int i, BTreeNode* y) {
	// Create a new node which is going to store (t-1) keys
	// of y
	BTreeNode* z = new BTreeNode(y->t, y->leaf);
	z->n = t - 1;

	// Copy the last (t-1) keys of y to z
	for (int j = 0; j < t - 1; j++)
		z->keys[j] = y->keys[j + t];

	// Copy the last t children of y to z
	if (y->leaf == false) {
		for (int j = 0; j < t; j++)
			z->C[j] = y->C[j + t];
	}

	// Reduce the number of keys in y
	y->n = t - 1;

	// Since this node is going to have a new child,
	// create space of new child
	for (int j = n; j >= i + 1; j--)
		C[j + 1] = C[j];

	// Link the new child to this node
	C[i + 1] = z;

	// A key of y will move to this node. Find the location of
	// new key and move all greater keys one space ahead
	for (int j = n - 1; j >= i; j--)
		keys[j + 1] = keys[j];

	// Copy the middle key of y to this node
	keys[i] = y->keys[t - 1];

	// Increment count of keys in this node
	n = n + 1;
}

// Driver program to test above functions
int main() {
	BTree t(3); // A B-Tree with minimum degree 3
	t.insert(10);
	t.insert(20);
	t.insert(5);
	t.insert(6);
	t.insert(12);
	t.insert(30);
	t.insert(7);
	t.insert(17);

	cout << "Traversal of the constructed tree is :";
	t.traverse();

	int k = 6;
	(t.search(k) != NULL) ? cout << "\nPresent" : cout << "\nNot Present";

	k = 15;
	(t.search(k) != NULL) ? cout << "\nPresent" : cout << "\nNot Present";

	return 0;
}
