#include <bits/stdc++.h>
using namespace std;
/**
 * 并查集
 * 1. 判断树根: p[x]==x
 * 2. 求x的集合编号 while(p[x]!=x) x=p[x];
 * 3. 合并两个集合(将x所属的集合合并到以y为根的集合): p[x]=y
 *
 */
const int N = 1e5 + 1;

int parent[N]; // parent[i]=x : 节点的父节点为x
void init() {
	for (int i = 1; i < N; ++i) {
		parent[i] = i;
	}
}

void union_(int p, int q) {
	parent[find(p)] = find(q);
}
/**
 * @brief 返回x的根节点 路径压缩
 * 
 * @param p  
 * @return int 
 */
int find(int p) {
	while (parent[p] != p) {
		parent[p] = parent[parent[p]];
		p = parent[p];
	}
	return p;
}
int main(int argc, char const* argv[]) {

	return 0;
}