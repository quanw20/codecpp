#ifndef __BINARY_TREE_H_
#define __BINARY_TREE_H_
#include <bits/stdc++.h>

template <class T>
struct TreeNode;

template <class T>
using Tree = TreeNode<T>*;
template <class T>
struct TreeNode {
	T val;
	Tree<T> left, right;
	TreeNode(T val) : val(val), left(nullptr), right(nullptr) {}
};
template <class T>
class BT {
private:
	Tree<T> root;
	Tree<T> insert(Tree<T> root, int val);
	Tree<T> search(Tree<T> root, int val);
	Tree<T> create(std::istream& in);
	void preOrder(Tree<T> root, std::function<void(Tree<T>)> func);
	int height(Tree<T> root) {
		return root ? std::max(height(root->left), height(root->right)) + 1 : 0;
	}
	int countLeaves(Tree<T> root);
	int countNodes(Tree<T> root);

public:
	BT() : root(nullptr) {}
	BT(std::istream& in) { root = create(in); }
	void insert(int val) { insert(root, val); }
	Tree<T> search(int val) { return search(root, val); }
	void preOrder();
	void inOrder();
	void postOrder();
	void levelOrder();
	void preOrder(std::function<void(Tree<T>)> func) { preOrder(root, func); }
	void exchange(Tree<T> root) {
		preOrder([](Tree<T> root) {
			Tree<T> temp = root->left;
			root->left = root->right;
			root->right = temp;
		});
	}
	int height() { return height(root); }
};
template <class T>
Tree<T> BT<T>::insert(Tree<T> root, int val) {
	if (!root)
		root = new TreeNode<T>(val);
	if (val > root->val)
		root->right = insert(root->right, val);
	if (val < root->val)
		root->left = insert(root->left, val);
	return root;
}
template <class T>
Tree<T> BT<T>::create(std::istream& in) {
	T ch;
	in >> ch;
	if (ch == '#')
		return nullptr;
	Tree<T> bt = new TreeNode<T>(ch);
	bt->left = create(in);
	bt->right = create(in);
	return bt;
}
template <class T>
Tree<T> BT<T>::search(Tree<T> root, int val) {
	if (root == nullptr || root->val == val)
		return root;
	if (val > root->val)
		return search(root->right, val);
	return search(root->left, val);
}
template <class T>
void BT<T>::preOrder(Tree<T> root, std::function<void(Tree<T>)> func) {
	if (root == nullptr)
		return;
	func(root);
	preOrder(root->left, func);
	preOrder(root->right, func);
}
template <class T>
void BT<T>::preOrder() {
	if (!root)
		return;
	std::stack<Tree<T>> s;
	Tree<T> bt = root;
	while (bt || !s.empty()) {
		while (bt) {
			std::cout << bt->val << ' ';
			s.push(bt);
			bt = bt->left;
		}
		if (!s.empty()) {
			bt = s.top()->right;
			s.pop();
		}
	}
	std::cout << '\n';
}
template <class T>
void BT<T>::inOrder() {
	if (!root)
		return;
	std::stack<Tree<T>> s;
	Tree<T> bt = root;
	while (bt || !s.empty()) {
		while (bt) {
			s.push(bt);
			bt = bt->left;
		}
		if (!s.empty()) {
			bt = s.top();
			s.pop();
			std::cout << bt->val << ' ';
			bt = bt->right;
		}
	}
	std::cout << '\n';
}
template <class T>
void BT<T>::postOrder() {
	Tree<T> bt = root;
	std::stack<std::pair<Tree<T>, int>> s;
	while (bt || !s.empty()) {
		while (bt) {
			s.push(std::pair<Tree<T>, int>(bt, 0));
			bt = bt->left;
		}
		while (!s.empty() && s.top().second) {
			bt = s.top().first;
			s.pop();
			std::cout << bt->val << ' ';
			bt = nullptr; // 必要否则无限循环
		}
		if (!s.empty()) {
			auto p = s.top();
			s.pop();
			p.second = 1;
			s.push(p);
			bt = p.first->right;
		}
	}
	std::cout << '\n';
}
template <class T>
void BT<T>::levelOrder() {
	if (!root)
		return;
	std::queue<Tree<T>> q;
	q.push(root);
	while (!q.empty()) {
		Tree<T> temp = q.front();
		q.pop();
		std::cout << temp->val << " ";
		if (temp->left)
			q.push(temp->left);
		if (temp->right)
			q.push(temp->right);
	}
	std::cout << '\n';
}
template <class T>
int BT<T>::countLeaves(Tree<T> root) {
	if (!root)
		return 0;
	if (!root->left && !root->right)
		return 1;
	return countLeaves(root->left) + countLeaves(root->right);
}
template <class T>
int BT<T>::countNodes(Tree<T> root) {
	if (!root)
		return 0;
	return countNodes(root->left) + countNodes(root->right) + 1;
}
#endif