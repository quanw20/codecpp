
#include <bits/stdc++.h>
using namespace std;

struct Node {
	int val, height;
	Node *left, *right;
	Node() : height(0) {}
	Node(int val) : val(val), left(nullptr), right(nullptr), height(1) {}
};

/**
 * @brief 到叶子节点的最大距离
 *
 * @param node
 * @return int
 */
int height(Node* node) { return node ? node->height : 0; }
/**
 * @brief
 *
 * @param node
 * @return int {-1, 0, 1} 平衡
 * @return int abs($)>1 不平衡
 */
int getBalance(Node* node) { return node ? height(node->left) - height(node->right) : 0; }
/**
 * @brief x的右子树y不平衡, 旋转后y变成根节点, x变成y的左子树
 * <pre>
 *   x             y
 * 	/ \           / \
 * T1 y    ==>    x  T3
 *   / \         / \
 *  T2 T3       T1 T2
 * </pre>
 * @param x (旋转前的)根节点
 * @return Node* 旋转后的根节点y
 */
Node* leftRotate(Node* x) {
	// 记录节点
	Node* y = x->right;
	Node* T2 = y->left;
	// 旋转
	y->left = x;
	x->right = T2;
	// 更新高度
	x->height = max(height(x->left), height(x->right)) + 1;
	y->height = max(height(y->left), height(y->right)) + 1;
	// 返回新的根节点
	return y;
}
/**
 * @brief y的左子树x不平衡, 旋转后x变成根节点y变成x的右子树
 * <pre>
 *     y              x
 *    / \       	 / \
 *    x  T3   ==>   T1  y
 *   / \               / \
 *  T1 T2             T2 T3
 * </pre>
 * @param y (旋转前的)根节点
 * @return Node* 旋转后的根节点x
 */
Node* rightRotate(Node* y) {
	Node* x = y->left;
	y->left = x->right;
	x->right = y;
	y->height = max(height(y->left), height(y->right)) + 1;
	x->height = max(height(x->left), height(x->right)) + 1;
	return x;
}
Node* adjust(Node* root) {

	return root;
}
/**
 * @brief 递归插入
 *
 * @param root 节点
 * @param val 值
 * @return Node* 子树的新节点
 */
Node* insert(Node* root, int val) {
	// 1. 执行正常的插入
	if (root == nullptr)
		return (new Node(val));
	if (val < root->val)
		root->left = insert(root->left, val);
	else if (val > root->val)
		root->right = insert(root->right, val);
	else // 不允许相同值
		return root;

	// 2.更新自身的高度
	root->height = max(height(root->left), height(root->right)) + 1;
	// 3.获取平衡因子
	int balance = getBalance(root);
	// 4.若abs(平衡因子)>1,有以下4种情况
	// 4.1 Left Left
	if (balance > 1 && getBalance(root->left) >= 0)
		return rightRotate(root);
	// 4.2 Right Right
	if (balance > 1 && getBalance(root->left) < 0) {
		root->left = leftRotate(root->left);
		return rightRotate(root);
	}
	// 4.3 Left Right
	if (balance < -1 && getBalance(root->right) <= 0)
		return leftRotate(root);
	// 4.4 Right Left
	if (balance < -1 && getBalance(root->right) > 0) {
		root->right = rightRotate(root->right);
		return leftRotate(root);
	}
	// 5. 返回根指针
	return root;
}

Node* minValueNode(Node* node) {
	while (node->left)
		node = node->left;
	return node;
}

/**
 * @brief 递归删除给定root中的给定val
 *
 * @param root 根
 * @param val 待删值
 * @return Node* 删除后的根节点
 */
Node* deleteNode(Node* root, int val) {

	// 1. 执行正常的删除
	if (!root) // 待删节点不存在
		return root;
	if (val < root->val)
		root->left = deleteNode(root->left, val);
	else if (val > root->val)
		root->right = deleteNode(root->right, val);
	else {								   //找到了
		if (!root->left || !root->right) { // 有0个或1个孩子
			Node* temp = root->left ? root->left : root->right;
			if (temp) { // 有一个孩子
				Node* p = root;
				root = temp;
				delete p;
			} else { // 没有孩子
				temp = root;
				root = nullptr;
				delete temp;
			}
		} else {											  // 有两个孩子
			Node* temp = minValueNode(root->right);			  // 获取后继
			root->val = temp->val;							  // 将后继赋给待删节点
			root->right = deleteNode(root->right, temp->val); //转换为删除后继
		}
	}
	// 2. 如果删除之后根节点为空就不需要调整了
	if (!root)
		return root;
	// 3. 更新节点高度
	root->height = 1 + max(height(root->left), height(root->right));
	return root;
}

// 遍历
void inOrder(Node* root) {
	if (root != nullptr) {
		inOrder(root->left);
		cout << root->val << '(' << root->height << ')' << " ";
		inOrder(root->right);
	}
}
void levelOrder(Node* root) {
	if (!root)
		return;
	queue<Node*> q;
	q.push(root);
	while (!q.empty()) {
		Node* t = q.front();
		q.pop();
		cout << t->val << '(' << t->height << ')' << ' ';
		if (t->left)
			q.push(t->left);
		if (t->right)
			q.push(t->right);
	}
}


int main() {
	Node* root = nullptr;

	root = insert(root, 10);
	root = insert(root, 20);
	root = insert(root, 30);
	root = insert(root, 40);
	root = insert(root, 50);
	root = insert(root, 25);

	cout << "Inorder traversal  \n";
	inOrder(root);
	cout << "\nLevel order traversal \n";
	levelOrder(root);
	deleteNode(root, 30);
	cout << "\nAfter delete:\n";
	cout << "inorder traversal \n";
	inOrder(root);
	cout << "\nLevel order traversal \n";
	levelOrder(root);
	return 0;
}
