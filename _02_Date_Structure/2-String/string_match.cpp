#include <bits/stdc++.h>
using namespace std;
/**
 * @brief brutal force 暴力
 *
 * @param str 字符串
 * @param pat 模式
 * @return int 首字母下标或-1表示没找到
 */
int bf(string str, string pat) {
	for (size_t i = 0; i < str.length(); i++) {
		size_t j = 0, k = i;
		for (; j < pat.size();) {
			if (str[k] == pat[j])
				++k, ++j;
			else
				break;
		}
		if (j == pat.size())
			return i;
	}
	return -1;
}



/**
 * @brief kmp 使用next数组
 *
 * @param str 字符串
 * @param pat 模式
 * @return int
 */
int kmp(string str, string pat) {
	const int N = pat.length();
	int next[N];
	// 初始胡next
	next[0] = -1;
	for (int k = 0, j = 1; j < N; ++j) {
		next[j] = k;
		while (k > 0 && pat[j] != pat[k])
			k = next[k];
		if (pat[j] == pat[k])
			++k;
	}
	// 匹配
	int i = 0, j = 0;
	for (; i < str.length();) {
		if (j == -1 || str[i] == pat[j])
			++j, ++i; // 成功++
		else
			j = next[j]; // 失败回溯
		if (j == pat.length())
			return i - j; // 首字母下标
	}
	return -1;
}
int kmp(const char* str, const char* pat) {
	// next
	int next[10001] = { 0 };
	for (int i = 1, j = 0; pat[i]; ++i) {
		while (j && pat[j] != pat[i])
			j = next[j];
		if (pat[i] == pat[j]) {
			++j;
			next[i] = j;
		}
	}
	// match
	for (int i = 0, j = 0; str[i]; ++i) {
		while (j && str[i] != pat[j])
			j = next[j];
		if (str[i] == pat[j])
			++j;
		if (!pat[j]) {
			cout << i - j + 1 << ' ';
			j = 0;
		}
	}
}
/**
 * @brief Get the "next" array
 *
 * @param next 数组
 * @param t 模式
 */
void get_next(int next[], string t) {
	next[0] = -1;
	for (int i = 0, j = -1; i < t.length() - 1;) {
		if (j == -1 || t[i] == t[j]) {
			i++;
			j++;
			next[i] = j;
		} else
			j = next[j];
	}
}

int main(int argc, char const* argv[]) {
	int a = bf("sdsdfaaregwr", "sdf");
	cout << a << '\n';
	a = bf("gsfwesgfvsfghj", "sdf");
	cout << a << '\n';
	string str = "ababababc";
	string pat = "ababc";
	int idx = kmp(str, pat);
	cout << idx;
	return 0;
}