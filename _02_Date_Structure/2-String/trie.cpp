#include <bits/stdc++.h>
using namespace std;
// trie树: 高效存储, 查找字符串
const int N = 1E5 + 1;
int son[N][26]; // 只有字母
int cnt[N];		// 以当前这个点结尾的单词有多少个
int idx;		// 当前用到的下标 下标为零的点既是根节点又是空节点
void insert(const char* str) {
	int p = 0;
	for (int i = 0; str[i]; ++i) {
		int u = str[i] - 'a'; // 当前字母对应的编号
		if (!son[p][u])		  // son[p][u]==0表示当前节点不存在
			son[p][u] = ++idx;
		p = son[p][u]; // 最终节点
	}
	++cnt[p];
}
int query(const char* str) {
	int p = 0;
	for (int i = 0; str[i]; ++i) {
		int u = str[i] - 'a';
		if (!son[p][u]) // 不存在
			return 0;
		p = son[p][u];
	}
	return cnt[p];
}
char str[N];



// i>=0 可以写成 ~i
// x>>2&1 x的第二位是0还是1
int main(int argc, char const* argv[]) {
	int n;
	scanf("%d", &n);
	while (n--) {
		char op[2];
		scanf("%s%s", op, str);
		if (op[0] == 'I')
			insert(str);
		else
			printf("%d\n", query(str));
	}
	return 0;
}
// 5
// I abc
// Q abc
// Q ab
// I ab
// Q ab