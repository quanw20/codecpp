#include <iostream>
#include "String.h"
using std::cout;

String s("tatatangtang");
String t("quanwei");
String r("tatang");
String u("");

void test_properitise() {
	cout << "\n- <<输出测试 -\n";
	cout << "s: " << s << '\n';
	cout << "t: " << t << '\n';
	cout << "r: " << r << '\n';
	cout << "u: " << u << '\n';

	cout << "\n- 长度测试 -\n";
	cout << "s.length() = " << s.length() << '\n';
	cout << "t.length() = " << t.length() << '\n';
	cout << "r.length() = " << r.length() << '\n';
	cout << "u.length() = " << u.length() << '\n';

	cout << "\n- 空串测试 -\n";
	cout << "s.isEmpty() = " << (s.isEmpty() ? "true" : "false") << '\n';
	cout << "t.isEmpty() = " << (t.isEmpty() ? "true" : "false") << '\n';
	cout << "r.isEmpty() = " << (r.isEmpty() ? "true" : "false") << '\n';
	cout << "u.isEmpty() = " << (u.isEmpty() ? "true" : "false") << '\n';

	cout << "\n- 比较测试 -\n";
	cout << "s.compareTo(s) = " << s.compareTo(s) << '\n';
	cout << "s.compareTo(t) = " << s.compareTo(t) << '\n';
	cout << "s.compareTo(r) = " << s.compareTo(r) << '\n';
	cout << "s.compareTo(u) = " << s.compareTo(u) << '\n';

	cout << "\n- 深拷贝测试 -\n";
	cout << "s.deepCopy() = " << s.deepCopy() << '\n';
}

void test_connect() {
	cout << "\n- 字符串连接测试 -\n";
	cout << "String::connect(t,u) = " << String::connect(t, r) << '\n';
	cout << "t = " << t << ", r= " << r << '\n';
	cout << "t.join(r) = " << t.join(r) << '\n';
	cout << "t = " << t << '\n';
}
void test_substring() {
	cout << "\n- 子串测试 -\n";
	cout << "s.substring(1,6)" << s.substring(1, 6) << '\n';
}
void test_indexOf() {
	cout << "\n- 子串查找测试 -\n";
	cout << "s.indexOf(t) = " << s.indexOf(t) << '\n';
	cout << "s.indexOf(r) = " << s.indexOf(r) << '\n';
	cout << "s.indexOf(u) = " << s.indexOf(u) << '\n';

	cout << "\n- 子串查找<静态方法>测试 -\n";
	cout << "String::indexOf(s,t) = " << String::indexOf(s, t) << '\n';
	cout << "String::indexOf(s,r) = " << String::indexOf(s, r) << '\n';

	cout << "\n- 子串查找<逆序>测试 -\n";
	cout << "s.lastIndexOf(u) = " << s.lastIndexOf(u) << '\n';
	cout << "s.lastIndexOf(u) = " << s.lastIndexOf(r) << '\n';
}
void test_insert_delete() {
	cout << "\n- 插入测试 -\n";
	cout << "String::insert(r, t, 3) = " << String::insert(r, t, 3) << '\n';
	cout << "t = " << t << ", r= " << r << '\n';
	cout << "r.insert(t,3) = " << r.insert(t, 3) << '\n';
	cout << "t = " << t << ", r= " << r << '\n';

	cout << "\n- 删除测试 -\n";
	cout << "r.deleted(3, 6) = " << r.deleted(3, 6) << '\n';
	cout << "t = " << t << ", r= " << r << '\n';
	cout << "r.del(3, 6) = " << r.del(3, 6) << '\n';
	cout << "t = " << t << ", r= " << r << '\n';
}

int main() {
	test_properitise();
	test_connect();
	test_substring();
	test_indexOf();
	test_insert_delete();
	return 0;
}
