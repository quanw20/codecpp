#include <bits/stdc++.h>
using namespace std;

/**
 * @brief
 *
 * 1. 暴力匹配怎么做
 * 2. 如何优化
 *
 * kmp
 *  next[i]=j
 * p[1,j]==p[i-j+1,i]
 *
 * @param argc
 * @param argv
 * @return int
 */
const int N = 1e5 + 1;
int m, n;
char p[N], s[N];
int nxt[N];
int main(int argc, char const* argv[]) {
	cin >> n >> p + 1 >> m >> s + 1;
	for (int i = 2, j = 0; i <= n; ++i) {
		while (j && p[i] != p[i + 1])
			j = nxt[j];
		if (p[i] == p[j + 1])
			++j;
		nxt[i] = j;
	}
	for (int i = 1, j = 0; i <= m; i++) {
		while (j && s[i] != p[j + 1])
			j = nxt[j];
		if (s[i] = p[j + 1])
			++j;
		if (j == n) {
			cout << i - n + 1 << ' ';// 下标从一开始
			j = nxt[j];// 从下一个位置继续
		}
	}
	return 0;
}
/*
4
asdf
20
asddaasdfasdfgqwdqasdfcqwacqwdasdqw
 */