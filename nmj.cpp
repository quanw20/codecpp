#include <bits/stdc++.h>
using namespace std;

template <class T>
struct ThrNode {
	T data;
	int ltag, rtag;
	ThrNode<T>*lchild, *rchild;
};
template <class T>
class InThrBiTree {
public:
	InThrBiTree();					 //构造函数，建立中序线索链表
									 //	ThrNode<T>*Pre(ThrNode<T>*p);//查找p的前驱
	ThrNode<T>* Next(ThrNode<T>* p); //查找p的后继
	void InOrder();					 //中序遍历线索链表
									 //	ThrNode<T>*Find(T e);
private:
	ThrNode<T>* Creat(ThrNode<T>* bt); //构造函数调用
	void ThrBiTree(ThrNode<T>* bt, ThrNode<T>* pre);
	void Release(ThrNode<T>* bt); //析构函数调用
								  //	ThrNode<T>*Find1(ThrNode<T>*bt,T e);
	ThrNode<T>* root;			  //指向线索链表的头指针
};

template <class T>
InThrBiTree<T>::InThrBiTree() {
	root = Creat(root);	   //建立带线索标志的二叉链表
	ThrBiTree(root, NULL); //遍历二叉链表，建立线索
}

template <class T>
ThrNode<T>* InThrBiTree<T>::Creat(ThrNode<T>* bt) {
	char ch;
	cin >> ch; //输入结点的数据信息，假设为字符
	if (ch == '#')
		bt = NULL; //建立一棵空树
	else {
		bt = new ThrNode<T>;
		bt->data = ch;
		bt->ltag = 0, bt->rtag = 0;
		bt->lchild = Creat(bt->lchild); //递归建立左子树
		bt->rchild = Creat(bt->rchild); //递归建立右子树
	}
	return bt;
}
template <class T>
void InThrBiTree<T>::ThrBiTree(ThrNode<T>* bt, ThrNode<T>* pre) {
	if (bt == NULL)
		return;
	ThrBiTree(bt->lchild, bt);
	if (bt->lchild == NULL) { // 没有左孩子, 对bt的左指针进行处理
		bt->ltag = 1;
		bt->lchild = pre; //设置bt的前驱线索为pre
	}
	if (bt->rchild == NULL) //对bt的右指针进行处理
		bt->rtag = 1;
	if (pre && pre->rtag == 1)
		bt->rchild=pre; //设置pre的后继线索
	ThrBiTree(bt->rchild, bt);
}
int main(int argc, char const* argv[]) {
	InThrBiTree<char> t;
	cout<<"ok";
	return 0;
}
