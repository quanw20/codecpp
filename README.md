# CODE_CPP

<a href='https://gitee.com/quanw20/codecpp'><img src='https://gitee.com/quanw20/codecpp/widgets/widget_6.svg' alt='Fork me on Gitee'></img></a>

This is all my learning of cpp

## 这是我的代码规范 C/C++

```c
#define BUF_SIZE 1024       // 宏定义全部大写,下划线分隔
constexpr ARRAY_LEN 265     // 常量全部大写,用下划线分隔

/*
变量命名要有意义
变量名尽量不缩写
作用域越大, 名字可以越长
*/
                            // 注释对齐
char* my_name = nullptr;    // 变量全部小写用下划线分隔
int g_flag;                 // 全局变量 g_xxx

class FileManage            // 类名大驼峰
{                           // 函数名一般用动宾，变量名一般用名词
privite:
    string m_name;          // 成员变量 m_xxx

public:
    bool isFind;            // 布尔变量应写成谓词
    static string s_name;   // 静态变量 s_xxx
    string getName();       // 小驼峰
    bool isExsit();
}

```

## 生成和调用动态 dll 库

1. windows 生成动态 dll 库的编译命令如下：

```bash
 g++ --share ai.cpp -o ai.dll
 #或者
 g++ -shared ai.cpp -o ai.dll
```

2. Linux 下生成相对应的文件，后缀一般为 so。编译指令与 windows 下类似，只是必须指定参数-fPIC，即“地址无关代码 Position Independent Code”。

```bash
g++ --share ai.cpp -fPIC -o ai.so
#或者
g++ -shared ai.cpp -fPIC -o ai.so
```

3. 调用生成的动态 dll 库
   执行下面的代码，演示如何调用生成的动态 dll 库

```bash
 g++ ai.dll demo.cpp -o demo.exe
```

## 目录结构:

```
Folder PATH listing
Volume serial number is B45F-1DFD
D:.
├───.vscode
├───lang
│   ├───basic
│   ├───brain_fuck
│   ├───file_exception
│   ├───functional
│   ├───math
│   ├───my_class
│   │   ├───Mystring
│   │   ├───String
│   │   └───vector
│   ├───recursive
│   ├───skill
│   └───stl_templet
├───mds
├───spy_control_system
├───Test
├───txt
├───_01_Algorithms
│   ├───backtracking
│   ├───bit_manipulation
│   ├───ciphers
│   ├───data_structures
│   │   └───cll
│   ├───dynamic_programming
│   ├───geometry
│   ├───graph
│   ├───graphics
│   ├───greedy_algorithms
│   ├───hashing
│   ├───linear_algebra
│   ├───machine_learning
│   ├───math
│   ├───numerical_methods
│   ├───operations_on_datastructures
│   ├───others
│   ├───probability
│   ├───range_queries
│   ├───search
│   ├───sorting
│   └───strings
└───_02_Date_Structure
    ├───1-LinearList
    │   ├───list
    │   │   ├───array_list
    │   │   └───linked_list
    │   ├───matrix
    │   ├───queue
    │   └───stack
    ├───2-String
    ├───3-Tree
    ├───4-Graph
    └───7-Design
```