#include <iostream>
#include <functional>
#include <algorithm>
#include <cmath>

void abssort(float* x, unsigned n) {
	std::sort(x, x + n,
		// Lambda expression begins
		[](float a, float b) {
			return (std::abs(a) < std::abs(b));
		} // end of lambda expression
	);
}

struct my_struct {
	int value;
	std::function<int(int)> function; // wrapped function object
									  // this can also wrap a closure (the result of evaluating a lambda expression)

	void apply() { value = function(value); } // use the stored function

	// use a function passed as the argument
	template <typename FN>
	void apply_this(FN fn) { value = fn(value); }

	friend std::ostream& operator<<(std::ostream& stm, const my_struct& ms) {
		return stm << "my_struct { value == " << ms.value << " }";
	}
};

struct SNode {
	int val;  // 节点值
	int next; // 指向下一个节点的指针
} sl[1001];	  // 这就是静态链表了
//当表达式中引导[] 非空时，采用这种方式传递 lambda 表达式是不安全的
// template <class FuncType> FuncType func
// std::function<返回类型（参数列表）>
void triversal(SNode sl[], int start, int end, std::function<void(int)> fun) {
	for (int i = start; i != end; i = sl[i].next)
		fun(sl[i].val);
}

int main() {
	// initialise an object with a (wrapped) lambda expression
	my_struct ms{ 23, [](int v) { return v * 2; } };
	std::cout << ms << '\n'; // my_struct { value == 23 }

	ms.apply();				 // use the stored function (23*2)S
	std::cout << ms << '\n'; // my_struct { value == 46 }

	ms.function = [](int v) { return v * v; }; // change the stored function
	ms.apply();								   // use it (46*46)
	std::cout << ms << '\n';				   // my_struct { value == 2116 }

	ms.apply_this([](int v) { return v * 3; }); // pass the function to be applied (2116*3)
	std::cout << ms << '\n';					// my_struct { value == 6348 }
}